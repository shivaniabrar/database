#ifndef UTILITY_H
#define UTILITY_H

#include "storage_mgr.h"

/* Helper functions */
extern SM_PageHandle createPage();
extern void deletePage(SM_PageHandle);

#endif