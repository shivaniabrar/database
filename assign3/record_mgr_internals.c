/**
  * Initializes internal datastructures for thr record manager
  */
#include <stdlib.h>
#include <string.h>

#include "record_mgr.h"
#include "record_mgr_internals.h"

//Returns record header size
static uint64_t RecordHeaderSize(Schema *schema);

//Init page header
char *
InitPageHeader(PageHeader *ph)
{
	ph->next_free_page = -1;
	ph->free_slot_start = 0;
	bzero(ph->padding,sizeof(uint64_t) * PAGE_HEADER_PADDING);
	return (char *)ph + sizeof(PageHeader);
}

//Init record header
int
InitRecordHeader(RecordHeader *rh)
{
	rh->allocated = false;
	return 0;
}

//Init Scan handle Mgmt data used by scan functions
int
InitScanHandleMgmtData(ScanHandleMgmtData *mgmtData, TableHeader *tableHeader, Expr *expr)
{
	mgmtData->expr = expr;
	mgmtData->rid.page = -1;
	mgmtData->rid.slot = -1;
	return 0;
}

/**
  * Initializes table header which stores table metadata information
  */
int
CreateTableHeader(TableHeader *tableHeader, Schema *schema)
{
	memset(tableHeader, 0, sizeof(*tableHeader));
	tableHeader->free_page_start = -1;
	tableHeader->recordSize = MAX(getRecordSize(schema),8);
	tableHeader->recordHeaderSize = RecordHeaderSize(schema);
	tableHeader->slots_in_page = (PAGE_SIZE - sizeof(PageHeader))/(tableHeader->recordSize + tableHeader->recordHeaderSize);
	tableHeader->num_tuples = 0;
	tableHeader->num_pages = RECORD_START_PAGE;
	return 0;
}

TableMgmtData *
CreateTableMgmtData()
{
	TableMgmtData *mgmtData;
	mgmtData = malloc(sizeof(TableMgmtData));
	mgmtData->bp = MAKE_POOL();
	return mgmtData;
}

static uint64_t
RecordHeaderSize(Schema *schema) 
{
	uint64_t size = 0;
	size += RESERVED_BITS;
	size += ((schema->numAttr-1) / (sizeof(char) * 8)) + 1;
	return size;
}