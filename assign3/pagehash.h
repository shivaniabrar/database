#include "page.h"
#include "hash.h"
#include "buffer_mgr.h"
#include "dt.h"

#ifndef PAGEHASH_H
#define PAGEHASH_H

/************************************************************
 *                    Interface                             *
 ************************************************************/
PageHead * SearchPage(HashTable htable,PageNumber pageNum);
void InsertPage(HashTable htable,PageHead *ph);
void RemovePage(HashTable htable,PageHead *ph);

/************************************************************
 *                    Utility                               *
 ************************************************************/
bool _search(Key key,Value value);

#endif