#ifndef RECORD_ALLOCATOR_H_
#define RECORD_ALLOCATOR_H_

#include "tables.h"
#include "record_mgr_internals.h"

RID AllocateRecord(TableMgmtData *mgmtData);
int FreeRecord(TableMgmtData *mgmtData, RID rid);
bool IsRecordExist(TableHeader *tableHeader, char *page, int slot);
uint64_t record_offset (TableHeader *tableHeader,uint64_t slot_number);
uint64_t slot_offset (TableHeader *tableHeader,uint64_t slot_number);
#endif