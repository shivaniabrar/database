#include <stdlib.h>
#include "pagehash.h"

bool _search(Key key,Value value){
	PageHead *ph;
	ph = GetEnclosingTypePtr(PageHead,listHash, value);
	if (key == ph->pageNum)
	{
		return true;
	}
	return false;
}

PageHead * SearchPage(HashTable htable,PageNumber pageNum){
	Value value;
	PageHead *ph=NULL;
	value = Search(htable,pageNum,_search);
	if (value!=NULL)
	{
		ph = GetEnclosingTypePtr(PageHead,listHash, value);
	}
	return ph;
}

void InsertPage(HashTable htable,PageHead *ph){
	Insert(htable,ph->pageNum,&ph->listHash);	
}

void RemovePage(HashTable htable,PageHead *ph){
	Remove(htable,&ph->listHash);
}