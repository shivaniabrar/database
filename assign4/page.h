#include "list.h"
#include "buffer_mgr.h"

#ifndef PAGE_H
#define PAGE_H

/************************************************************
 *                    Data structures                       *
 ************************************************************/

typedef struct PageHead {
    PageNumber pageNum;
    char *data;
    bool dirty;
 	int index;
    unsigned int fixedCount;
    unsigned long frequency;
    ListNode listLink;
    ListNode listHash;
    bool used;
}PageHead;

#endif
