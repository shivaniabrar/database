#ifndef BTREE_MGR_INTERNALS_H_
#define BTREE_MGR_INTERNALS_H_
#include <stdlib.h>
#include <stdint.h>

#include "buffer_mgr.h"
#include "tables.h"
#include "btree_mgr.h"

#define BTREE_HEADER_PAGE 0
#define ROOT_NODE 1
#define NOT_INIT 0

#define NON_LEAF_NODE FALSE
#define LEAF_NODE TRUE

#define NEW_SPLIT_NODE 0
#define OLD_NODE 1

/*
	Data Structures
*/

typedef struct BTreeHeader {
	uint64_t root;
	uint64_t numNodes;
	uint64_t numEntries;
	uint64_t fanOut;
} BTreeHeader;

typedef struct BTreeMgmtData {
	BTreeHeader btreeHeader;
	BM_BufferPool *bp;
} BTreeMgmtData;

typedef struct NodeHeader{
	bool isLeafNode;
	int pageNum;
	uint64_t numKeys;
	uint64_t nextNode;
	uint64_t prevNode;
	uint64_t parentNode; 
} NodeHeader;

typedef struct LeafNodeEntry {
	Value key;
	RID rid; 
} LeafNodeEntry;

typedef struct NonLeafNodeEntry {
	Value key;
	uint64_t pointer;
} NonLeafNodeEntry;

typedef struct ShouldInsert {
	bool should_insert;
	NonLeafNodeEntry non_leaf_node_entry;
} ShouldInsert;

typedef LeafNodeEntry* LeafNode;
typedef NonLeafNodeEntry* NonLeafNode;

typedef struct IndexScanHandleMgmtData  {
	uint64_t leafNode;
	int64_t  key_position;
}IndexScanHandleMgmtData;

/*
	Functions
*/
int CreateBtreeHeader(BTreeHeader *btreeHeader, uint64_t fanOut);
int CreateBTreeMgmtData(BTreeMgmtData **mgmtData, char *idxId);
int DestroyBtreeMgmtData(void *mgmtData);
int InitNodeHeader (void *page, bool isLeaf, int pageNum);
ShouldInsert _insert(void *mgmtData, Value *key, RID rid, uint64_t node);
RID findKeyInLeafNode(NodeHeader *nh, Value *key);
uint64_t findNearestKey(NodeHeader *nh, Value *key);
IndexScanHandleMgmtData * CreateIndexScanMgmtData(void *mgmtData);
void _deletekey(BTreeHeader *btreeHeader,Value *key);
#endif