#ifndef TRANSCODER_H_
#define TRANSCODER_H_

#include "tables.h"
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "record_mgr_internals.h"
#include "recordAllocator.h"

#define ENCODE(buffer, source) \
	({ \
		memcpy(buffer, &(source), sizeof(typeof(source)));\
		buffer + sizeof(typeof(source));\
	})

#define ENCODE_STRING(buffer, source) \
	({ \
		*((uint64_t *)buffer) = strlen(source);\
		strncpy(buffer + sizeof(uint64_t), source, strlen(source));\
		buffer + sizeof(uint64_t) + strlen(source);\
	})

#define DECODE(buffer, destination) \
	({ \
		memcpy(&(destination), buffer, sizeof(typeof(destination)));\
		buffer + sizeof(typeof(destination));\
	})

#define DECODE_STRING(buffer, destination) \
	({ \
		uint64_t len = 0;\
		buffer = DECODE(buffer,len);\
		destination = malloc(len+1);\
		bzero(destination,len+1);\
		memcpy(destination, buffer, len);\
		buffer + len;\
	})

char * EncodeSchema(char *buffer_ptr, Schema *schema);
char * DecodeSchema(char *buffer_ptr, Schema *schema);
char * EncodeRecord(TableHeader *tableHeader, char *page, Record *record);
char * DecodeRecord(TableHeader *tableHeader, char *page, Record *record);
#endif