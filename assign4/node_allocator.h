#ifndef NODE_ALLOCATOR_H_
#define NODE_ALLOCATOR_H_

#include <stdint.h>

#include "buffer_mgr.h"
#include "btree_mgr_internals.h"

uint64_t AllocateNode(void *btreeMgmtData, int type);

#endif