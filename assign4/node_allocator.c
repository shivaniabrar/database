#include "node_allocator.h"


uint64_t
AllocateNode(void *mgmtData, int type)
{
	BTreeMgmtData *btreeMgmtData = mgmtData;
	BTreeHeader *btreeHeader = &(btreeMgmtData->btreeHeader);
	BM_BufferPool *bp = btreeMgmtData->bp;
	uint64_t pageNum;
	BM_PageHandle *ph = NULL;
	
	ph = MAKE_PAGE_HANDLE();
	
	pinPage(bp,ph,btreeHeader->numNodes);
	
	InitNodeHeader(ph->data,type,ph->pageNum);
	
	markDirty(bp,ph);
	unpinPage(bp,ph);

	pageNum = btreeHeader->numNodes;
	++btreeHeader->numNodes;
	
	free(ph);
	return pageNum;
}