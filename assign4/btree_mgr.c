#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "btree_mgr.h"
#include "transcoder.h"
#include "buffer_mgr.h"
#include "btree_mgr_internals.h"
#include "storage_mgr.h"
#include "node_allocator.h"

char *_printTree (BTreeHandle *tree, uint64_t node);
char *printNonLeafNode(NodeHeader *nh);
char *printLeafNode(NodeHeader *nh);

// init and shutdown index manager
RC
initIndexManager (void *mgmtData)
{
	return RC_OK;
}

RC
shutdownIndexManager ()
{
	return RC_OK;
}

// create, destroy, open, and close an btree index
/*
	Creates Pagefile for B+tree 
	Inits BtreeHeader
	Stores BtreeHeader into the disk
*/
RC
createBtree (char *idxId, DataType keyType, int n)
{
	RC status = RC_OK;
	BTreeHeader btreeHeader;
	BM_BufferPool *bp = NULL;
	BM_PageHandle *ph = NULL;
	char *page = NULL;

	//Creates Page file
	status = createPageFile(idxId);
	if (status != RC_OK) {
		return status;
	}

	//Inits BtreeHeader
	CreateBtreeHeader(&btreeHeader,n);

	ph = MAKE_PAGE_HANDLE();
	bp = MAKE_POOL();

	//Inits Buffer Pool
	initBufferPool(bp, idxId, 2, RS_FIFO, NULL);

	//Pins page
	pinPage(bp,ph,BTREE_HEADER_PAGE);
	page = ph->data;

	//Encodes key and btreeHeader to page
	page = (char *)ENCODE(page, keyType);
	page = (char *)ENCODE(page, btreeHeader);

	markDirty(bp,ph);
	unpinPage(bp,ph);

	shutdownBufferPool(bp);
	free(bp);
	free(ph);	
	return status;
}

/*
	Opens Page file
	Loads Btree Header from disk
*/
RC
openBtree (BTreeHandle **tree, char *idxId)
{
	RC status = RC_OK;
	BTreeHandle *btree = NULL;
	BTreeMgmtData *mgmtData = NULL;
	char *page = NULL;
	BM_PageHandle *ph = NULL;

	btree = (BTreeHandle *)malloc(sizeof(BTreeHandle));
	memset(btree,0,sizeof(BTreeHandle));

	//Inits Btree MgmtData
	status = CreateBTreeMgmtData(&mgmtData, idxId);
	if (status != RC_OK) {
		return status;
	}
	btree->mgmtData = mgmtData;
	
	ph = MAKE_PAGE_HANDLE();
	pinPage(mgmtData->bp,ph,BTREE_HEADER_PAGE);
	page = ph->data;

	//Loads keytype and btreeHeader from disk
	page = (char *)DECODE(page, btree->keyType);
	page = (char *)DECODE(page,mgmtData->btreeHeader);

	markDirty(mgmtData->bp,ph);
	unpinPage(mgmtData->bp,ph);	
	
	free(ph);
	*tree = btree;
	return status;	
}

/*
	De-allocates in-memory Datastructures
*/
RC
closeBtree (BTreeHandle *tree)
{
	//De-allocates Btree Mgmt Data 
	DestroyBtreeMgmtData(tree->mgmtData);
	free(tree);
	return RC_OK;
}

/*
	Deletes Page file
*/
RC
deleteBtree (char *idxId)
{
	int status = RC_OK;
	//Delete Page file
	status = destroyPageFile(idxId);
	return status;
}

// access information about a b-tree
/*
	Gets number of nodes stored in B+tree
*/
RC
getNumNodes (BTreeHandle *tree, int *result)
{
	BTreeMgmtData *mgmtData = tree->mgmtData;
	BTreeHeader *btreeHeader = &mgmtData->btreeHeader;
	*result = btreeHeader->numNodes-1;
	return RC_OK;	
}

/*
	Gets number of entries stored in B+tree
*/
RC
getNumEntries (BTreeHandle *tree, int *result)
{
	BTreeMgmtData *mgmtData = tree->mgmtData;
	BTreeHeader *btreeHeader = &mgmtData->btreeHeader;
	*result = btreeHeader->numEntries;
	return RC_OK;
}

/*
	Gets number of entries stored in B+tree
*/
RC
getKeyType (BTreeHandle *tree, DataType *result)
{
	*result = tree->keyType;
	return RC_OK;
}

// index access
/*
	Finds Key in B+tree
*/
RC
findKey (BTreeHandle *tree, Value *key, RID *result)
{
	
	BTreeMgmtData *mgmtData = tree->mgmtData;
	BM_BufferPool *bp = mgmtData->bp;
	BM_PageHandle ph;
	NodeHeader *nh = NULL;

	uint64_t node = mgmtData->btreeHeader.root;

	do {	
		pinPage(bp,&ph,node);
		nh = (NodeHeader *)ph.data;	
		if (nh->isLeafNode) {
			//finds key in leaf node
			*result = findKeyInLeafNode(nh, key);
			unpinPage(bp,&ph);
			break;
		}
		//Finds key in non-leaf node and gets pointer 
		node = findNearestKey(nh,key);
		unpinPage(bp,&ph);
	}while(TRUE);
	
	return RC_OK;
}


/*
	Inserts Key in B+tree
*/
RC
insertKey (BTreeHandle *tree, Value *key, RID rid)
{
	BTreeMgmtData *mgmtData = tree->mgmtData;
	BTreeHeader *btreeHeader = &mgmtData->btreeHeader;
	ShouldInsert should_insert;
	NonLeafNode non_leaf_node;
	NodeHeader *nh;
	BM_PageHandle ph;

	int newRoot = NOT_INIT;

	//Allocate node if root not initialize
	if (btreeHeader->root == NOT_INIT) {
		AllocateNode(mgmtData, LEAF_NODE);
		btreeHeader->root = ROOT_NODE;
	}

	//Inserts key in tree
	should_insert = _insert(mgmtData, key, rid, btreeHeader->root);
	//If new root due to spliting then
		//init new root 
	if (should_insert.should_insert == TRUE) {
		newRoot = AllocateNode(mgmtData, NON_LEAF_NODE);
		pinPage(mgmtData->bp, &ph, newRoot);
		nh = (NodeHeader *)ph.data;
		non_leaf_node = (NonLeafNode)(nh+1);
		non_leaf_node[0].pointer = btreeHeader->root;
		non_leaf_node[1] = should_insert.non_leaf_node_entry;
		nh->numKeys = 2;
		btreeHeader->root = newRoot;
		markDirty(mgmtData->bp, &ph);
		unpinPage(mgmtData->bp, &ph);
	}
	
	return RC_OK;
}

/*
	Deletes Key in B+tree
*/
RC
deleteKey (BTreeHandle *tree, Value *key)
{
	BTreeMgmtData *mgmtData = tree->mgmtData;
	BTreeHeader *btreeHeader = &mgmtData->btreeHeader;
	_deletekey(btreeHeader,key);
	return RC_OK;
}

/*
	Inits Scan Handle
*/
RC
openTreeScan (BTreeHandle *tree, BT_ScanHandle **handle)
{
	BT_ScanHandle *sc = (BT_ScanHandle *)malloc(sizeof(BT_ScanHandle));
	sc->tree = tree;
	//Allocates and Inits scanhandle
	sc->mgmtData = CreateIndexScanMgmtData(tree->mgmtData);
	*handle = sc;
	return RC_OK;
}

/*
	Gets next entry
*/
RC
nextEntry (BT_ScanHandle *handle, RID *result)
{
	IndexScanHandleMgmtData *scmgmtData = handle->mgmtData;
	BTreeMgmtData *mgmtData = handle->tree->mgmtData;
	
	uint64_t node = scmgmtData->leafNode;
	int64_t key_position = scmgmtData->key_position;

	BM_BufferPool *bp = mgmtData->bp;
	BM_PageHandle ph;

	NodeHeader *nh = NULL;
	LeafNode leafNode; 

	//If there are no more entries return
	if (node == NOT_INIT) {
		return RC_IM_NO_MORE_ENTRIES;
	}

	pinPage(bp,&ph,node);
	nh = (NodeHeader *)ph.data;
	leafNode = (LeafNode)(nh+1);

	//Get key from leafNode
	*result = leafNode[key_position].rid;

	//Increase key position
	++scmgmtData->key_position;
	
	//If all the keys from leafNode is visited 
		//gets next leafnode
	if (scmgmtData->key_position == nh->numKeys) {
		scmgmtData->key_position = 0;
		scmgmtData->leafNode = nh->nextNode;
	}

	unpinPage(bp,&ph);
	return RC_OK;
}

/*
	De-allocates datastructures associated with Scanning
*/
RC
closeTreeScan (BT_ScanHandle *handle)
{
	free(handle->mgmtData);
	free(handle);
	return RC_OK;
}


// debug and test functions
char *printTree (BTreeHandle *tree)
{
	BTreeMgmtData *mgmtData = tree->mgmtData;
	BTreeHeader *btreeHeader = &mgmtData->btreeHeader;
	_printTree(tree,btreeHeader->root);
	return NULL;
}

/*
	Recursive version of print tree
*/
char *_printTree (BTreeHandle *tree, uint64_t node)
{
	BTreeMgmtData *mgmtData = tree->mgmtData;
	NonLeafNode non_leaf_node;
	NodeHeader *nh;
	BM_PageHandle ph;
	int i;
	BM_BufferPool *bp = mgmtData->bp;
	pinPage(bp,&ph,node);
	nh = (NodeHeader *)ph.data;
	if (!nh->isLeafNode) {
		non_leaf_node = (NonLeafNode)(nh+1);
		printNonLeafNode(nh);
		for (i = 0; i < nh->numKeys; ++i){
			_printTree(tree,non_leaf_node[i].pointer);
		}
	}else {
		printLeafNode(nh);
	}
	unpinPage(bp,&ph);
	return NULL;
}

/*
	Prints non-leaf node
*/
char *printNonLeafNode(NodeHeader *nh)
{
	NonLeafNode  non_leaf_node = (NonLeafNode)(nh+1);
	int i;
	printf("NonLeafPage %d: ", nh->pageNum);
	for (i = 0; i < nh->numKeys; ++i) {
		printf("%d:%d ", (int)non_leaf_node[i].pointer, non_leaf_node[i].key.v.intV);
	}
	printf("\n");
	return NULL;
}

/*
	Prints Leaf node
*/
char *printLeafNode(NodeHeader *nh)
{
	LeafNode  leaf_node = (LeafNode)(nh+1);
	int i;
	printf("LeafPage %d: ", nh->pageNum);
	for (i = 0; i < nh->numKeys; ++i) {
		printf("%d.%d %d ", leaf_node[i].rid.page,leaf_node[i].rid.slot, leaf_node[i].key.v.intV);
	}
	printf("\n");
	return NULL;
}