#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

#include "storage_mgr.h"
#include "dberror.h"
#include "utility.h"

#define MINIMUM_PAGE_COUNT 1
#define OR ||
#define AND &&

/* Private helper functions */
static int isPageFileCorrupted(int,int); 

/* Main Routines */
void initStorageManager (void){}

RC createPageFile (char *fileName)
{
	int fd;
	SM_PageHandle totalNumPages_page;
	SM_PageHandle empty_page;
	

	if ((fd = open(fileName, O_RDWR|O_CREAT, 0666)) < 0){
		return RC_FILE_NOT_FOUND;
	}

	/* Create page in memory */
	totalNumPages_page = createPage();
	empty_page = createPage();
	
	/* Intialize totalNumPages */
	sprintf(totalNumPages_page,"%d",MINIMUM_PAGE_COUNT);
		
	/* Move to approriate page */
	lseek(fd,(PAGE_SIZE)*sizeof(char),SEEK_SET);
	write(fd,empty_page,PAGE_SIZE);

	/* Ensuring page is added to the file */
	fsync(fd);

	lseek(fd,0,SEEK_SET);
	write(fd,totalNumPages_page,PAGE_SIZE);
			
	close(fd);
	
	/* Delete page from memory */
	deletePage(totalNumPages_page);
	deletePage(empty_page);

	return RC_OK;
}

RC openPageFile (char *fileName, SM_FileHandle *fHandle)
{
	int fd;
	SM_PageHandle totalNumPages_page;
	int totalNumPages;

	if ( (fd = open(fileName, O_RDWR, 0666)) < 0){
		return RC_FILE_NOT_FOUND;
	}

	totalNumPages_page = createPage();
	read(fd,totalNumPages_page,PAGE_SIZE);
	totalNumPages = atoi(totalNumPages_page);

	deletePage(totalNumPages_page);

	if (isPageFileCorrupted(fd,totalNumPages) == 1)
	{
		close(fd);
		return RC_FILE_NOT_FOUND;
	}

	/* Intialize fHandle */
	fHandle->fileName = fileName;
	fHandle->totalNumPages= totalNumPages;
	fHandle->curPagePos=0;
	fHandle->mgmtInfo = (void *)(intptr_t)fd;

	return RC_OK;
}

RC closePageFile (SM_FileHandle *fHandle){
	int status;
	status = close((int)(intptr_t)fHandle->mgmtInfo);
	if (status == 0)
	{
		return RC_OK;
	}
	return RC_FILE_NOT_FOUND;
}

RC destroyPageFile (char *fileName){
	int status;
	status = remove(fileName);
	if(status == 0){
		return RC_OK;
	}
	return RC_FILE_NOT_FOUND;
}

/* reading blocks from disc */
RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage)
{
	int totalNumPages;
	int fd;
	char *mgmtInfo;

	mgmtInfo = fHandle->mgmtInfo;
	fd = (int)(intptr_t)fHandle->mgmtInfo;
	totalNumPages = fHandle->totalNumPages;

	if ( mgmtInfo == NULL ) {
		return RC_FILE_HANDLE_NOT_INIT;
	}

	if ( (pageNum<0) OR (pageNum>totalNumPages) ){
		return RC_READ_NON_EXISTING_PAGE;
	}

	/* Move to approriate page */
	lseek(fd,(PAGE_SIZE)*(pageNum+1)*sizeof(char),SEEK_SET);
	
	/* Read and store page in memPage */
	read(fd,memPage,PAGE_SIZE);

	/* Update current page position */
	fHandle->curPagePos= pageNum;

	return RC_OK;
}

int getBlockPos (SM_FileHandle *fHandle){
	return fHandle->curPagePos;
}

RC readFirstBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
	return readBlock(0,fHandle,memPage);
}

RC readPreviousBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
	return readBlock(fHandle->curPagePos-1,fHandle,memPage);
}

RC readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
	return readBlock(fHandle->curPagePos,fHandle,memPage);	
}

RC readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
	return readBlock(fHandle->curPagePos+1,fHandle,memPage);
}

RC readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
	return readBlock(fHandle->totalNumPages-1,fHandle,memPage);
}

/* writing blocks to a page file */
RC writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage)
{
	int totalNumPages;
	int fd;
	char *mgmtInfo;

	totalNumPages = fHandle->totalNumPages;
	fd = (int)(intptr_t)fHandle->mgmtInfo;
	mgmtInfo = fHandle->mgmtInfo;

	if ( mgmtInfo == NULL ) {
		return RC_FILE_HANDLE_NOT_INIT;
	}

	if ( (pageNum<0) OR (pageNum>totalNumPages) ){
		return RC_WRITE_NON_EXISTING_PAGE;
	}

	lseek(fd,(PAGE_SIZE)*(pageNum+1)*sizeof(char),SEEK_SET);
	write(fd,memPage,PAGE_SIZE);
	fHandle->curPagePos= pageNum;
	return RC_OK;
}

RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
	return writeBlock(fHandle->curPagePos,fHandle,memPage);
}

RC appendEmptyBlock (SM_FileHandle *fHandle)
{

	SM_PageHandle totalNumPages_page;
	SM_PageHandle empty_page;
	int fd;
	int totalNumPages;


	totalNumPages_page = createPage();
	empty_page = createPage();

	/* Get file descriptor */
	fd = (int)(intptr_t)fHandle->mgmtInfo;

	/* Update total number of pages */
	fHandle->totalNumPages += 1; 
	totalNumPages = fHandle->totalNumPages;
	
	sprintf(totalNumPages_page,"%d",totalNumPages);
		
	/* Append empty page*/
	lseek(fd,(PAGE_SIZE)*(totalNumPages),SEEK_SET);
	write(fd,empty_page,PAGE_SIZE);

	/* Ensuring page is appended to the file */
	fsync(fd);

	/* Update total number of pages in a page file */
	lseek(fd,0,SEEK_SET);
	write(fd,totalNumPages_page,PAGE_SIZE);

	/* Update current position */
	fHandle->curPagePos = totalNumPages-1;

	deletePage(totalNumPages_page);
	deletePage(empty_page);

	return RC_OK;
}

RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle){
	int i;
	int totalNumPages;
	int numofpagestoadd;

	totalNumPages = fHandle->totalNumPages;
	numofpagestoadd = numberOfPages-totalNumPages;


	if ( numofpagestoadd < 0 ){
		return RC_INVALID_ARGUMENT;
	}

	for (i=0; i<numofpagestoadd; i++){
			appendEmptyBlock(fHandle);
	}
	
	return RC_OK;
}

int isPageFileCorrupted(int fd,int totalNumPages){
  struct stat filestat;
  int expectedtotalNumPages;
  __off_t filesize;

  fstat(fd,&filestat);
  filesize = filestat.st_size;
  expectedtotalNumPages = filesize/PAGE_SIZE - MINIMUM_PAGE_COUNT;
  
  if(expectedtotalNumPages != totalNumPages) {
    return 1;
  }

  return 0;
}