#ifndef RECORD_MANAGER_INTERNALS_H_
#define RECORD_MANAGER_INTERNALS_H_

#include <stdint.h>
#include <strings.h>

#include "expr.h"
#include "buffer_mgr.h"
#include "tables.h"
#include "btree_mgr.h"

#define SCHEMA_PAGE 0
#define TABLE_HEADER_PAGE 1
#define RECORD_START_PAGE 2 

#define TABLE_HEADER_PADDING 3
#define PAGE_HEADER_PADDING 6
#define RESERVED_BITS 1

typedef struct TableHeader
{
	uint64_t slots_in_page;
	uint64_t free_page_start;
	uint64_t num_tuples;
	uint64_t recordSize;
	uint64_t recordHeaderSize;
	uint64_t num_pages;
	uint64_t padding[TABLE_HEADER_PADDING]; 
}TableHeader;

typedef struct PageHeader
{
	uint64_t next_free_page;
	uint64_t free_slot_start;
	uint64_t padding[PAGE_HEADER_PADDING];
}PageHeader;

typedef struct TableMgmtData
{
	TableHeader tableHeader;
	BM_BufferPool *bp;
	BTreeHandle *tree;		
}TableMgmtData;

typedef struct RecordHeader
{
	char allocated : 1;
	char null[];
}RecordHeader;

typedef struct ScanHandleMgmtData
{
	Expr *expr;
	RID  rid;
	BTreeHandle *tree;
}ScanHandleMgmtData;

typedef struct ContestMgmtData
{
	uint64_t num_pages;
	BM_BufferPool *bp;
}ContestMgmtData;

char *InitPageHeader(PageHeader *ph);
int CreateTableHeader(TableHeader *tableHeader, Schema *schema);
TableMgmtData *CreateTableMgmtData(void);
int InitRecordHeader(RecordHeader *rh);
int InitScanHandleMgmtData(ScanHandleMgmtData *mgmtData, TableHeader *tableHeader, Expr *expr, BTreeHandle *tree);
int InitContestMgmtData(ContestMgmtData *contestMgmtData, int num_pages);
#endif
