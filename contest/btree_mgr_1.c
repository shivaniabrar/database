#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "btree_mgr.h"
#include "transcoder.h"
#include "buffer_mgr.h"
#include "btree_mgr_internals.h"
#include "storage_mgr.h"
#include "node_allocator.h"

char *_printTree (BTreeHandle *tree, uint64_t node);
char *printNonLeafNode(NodeHeader *nh);
char *printLeafNode(NodeHeader *nh);

// init and shutdown index manager
RC
initIndexManager (void *mgmtData)
{
	return RC_OK;
}

RC
shutdownIndexManager ()
{
	return RC_OK;
}

// create, destroy, open, and close an btree index
/*
	Creates Pagefile for B+tree 
	Inits BtreeHeader
	Stores BtreeHeader into the disk
*/
RC
createBtree (char *idxId, DataType keyType, int n)
{
	return RC_OK;
}

/*
	Opens Page file
	Loads Btree Header from disk
*/
RC
openBtree (BTreeHandle **tree, char *idxId)
{
	return RC_OK;	
}

/*
	De-allocates in-memory Datastructures
*/
RC
closeBtree (BTreeHandle *tree)
{
	//De-allocates Btree Mgmt Data 
	return RC_OK;
}

/*
	Deletes Page file
*/
RC
deleteBtree (char *idxId)
{
	int status = RC_OK;
	return status;
}

// access information about a b-tree
/*
	Gets number of nodes stored in B+tree
*/
RC
getNumNodes (BTreeHandle *tree, int *result)
{
	return RC_OK;	
}

/*
	Gets number of entries stored in B+tree
*/
RC
getNumEntries (BTreeHandle *tree, int *result)
{
	return RC_OK;
}

/*
	Gets number of entries stored in B+tree
*/
RC
getKeyType (BTreeHandle *tree, DataType *result)
{
	return RC_OK;
}

// index access
/*
	Finds Key in B+tree
*/
RC
findKey (BTreeHandle *tree, Value *key, RID *result)
{
	return RC_OK;
}


/*
	Inserts Key in B+tree
*/
RC
insertKey (BTreeHandle *tree, Value *key, RID rid)
{	
	return RC_OK;
}

/*
	Deletes Key in B+tree
*/
RC
deleteKey (BTreeHandle *tree, Value *key)
{
	return RC_OK;
}

/*
	Inits Scan Handle
*/
RC
openTreeScan (BTreeHandle *tree, BT_ScanHandle **handle)
{
	return RC_OK;
}

/*
	Gets next entry
*/
RC
nextEntry (BT_ScanHandle *handle, RID *result)
{
	return RC_IM_NO_MORE_ENTRIES;
}

/*
	De-allocates datastructures associated with Scanning
*/
RC
closeTreeScan (BT_ScanHandle *handle)
{
	return RC_OK;
}


// debug and test functions
char *printTree (BTreeHandle *tree)
{
	return NULL;
}