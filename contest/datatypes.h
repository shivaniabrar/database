#ifndef DATATYPE_H_
#define DATATYPE_H_

#include "tables.h"

typedef struct dataTypes{
	int (*setAttrValue)(void *attrptr,Value *value);
	int (*getAttrValue)(Schema *schema,void *attrptr,Value *value, int attr);
	int (*size)(Schema *schema,int attr);
}dataTypes_t;

extern dataTypes_t *dataTypes_table[NO_OF_DATA_TYPES]; 

#endif