#include "btree_mgr_internals.h"
#include <string.h>
#include <assert.h>
#include "node_allocator.h"

static void place_in_node(NodeHeader *nh, BTreeHeader *btreeHeader, Value *key, RID rid);
static ShouldInsert place_key_rid(NodeHeader *nh, BTreeMgmtData *btree_mgmtData, Value *key, RID rid);
static bool SpaceAvailable(NodeHeader *nh,BTreeHeader *btreeHeader);
static NonLeafNodeEntry split_leaf_node(NodeHeader *nh, BTreeMgmtData *btree_mgmtData, Value *key, RID rid);

static NonLeafNodeEntry split_non_leaf_node(NodeHeader *nh, BTreeMgmtData *btree_mgmtData, NonLeafNodeEntry new_entry);
static bool SpaceAvailableInNonLeafNode(NodeHeader *nh,BTreeHeader *btreeHeader);
static void place_in_non_leaf_node(NodeHeader *nh, BTreeHeader *btreeHeader, NonLeafNodeEntry non_leaf_node_entry, int new_split_node);
static ShouldInsert place_key_pointer(NodeHeader *nh, BTreeMgmtData *btree_mgmtData, NonLeafNodeEntry new_entry);

/*
	Inits btreeHeader
*/
int
CreateBtreeHeader(BTreeHeader *btreeHeader, uint64_t fanOut)
{
	bzero(btreeHeader,sizeof(BTreeHeader));
	btreeHeader->root = NOT_INIT;
	btreeHeader->numNodes = ROOT_NODE;
	btreeHeader->numEntries = 0;
	btreeHeader->fanOut = fanOut;
	return 0;
}

/*
	Allocates & Inits BtreeMgmtData
*/
int
CreateBTreeMgmtData(BTreeMgmtData **mgmtData, char *idxId)
{
	BTreeMgmtData *btree_mgmtData = (BTreeMgmtData *)malloc(sizeof(BTreeMgmtData));
	uint64_t status = 0; 
	bzero(btree_mgmtData,sizeof(BTreeMgmtData));
	bzero(&btree_mgmtData->btreeHeader,sizeof(BTreeHeader));

	*mgmtData = btree_mgmtData;
	btree_mgmtData->bp = MAKE_POOL();
	status = initBufferPool(btree_mgmtData->bp,idxId,1024,RS_FIFO,NULL);
	return status;
}

/*
	Allocates & Inits BtreeMgmtData
*/
int
DestroyBtreeMgmtData(void *mgmtData)
{
	int status = RC_OK;
	BTreeMgmtData *btree_mgmtData = mgmtData;
	status = shutdownBufferPool(btree_mgmtData->bp);
	free(btree_mgmtData->bp);
	free(mgmtData);
	return status;
}

/*
	Inits Node Header
*/
int 
InitNodeHeader (void *page, bool isLeaf, int pageNum)
{
	NodeHeader *nh = page;	
	nh->isLeafNode = isLeaf;
	nh->numKeys = 0;
	nh->nextNode = 0;
	nh->prevNode = pageNum;
	nh->parentNode = pageNum;
	nh->pageNum = pageNum; 
	return 0;
}

/*
	Inserts in B+-tree
*/
ShouldInsert
_insert(void *mgmtData, Value *key, RID rid, uint64_t node)
{
	BTreeMgmtData *btree_mgmtData = mgmtData;
	NodeHeader *nh = NULL;
	BM_PageHandle *ph = NULL;
	ShouldInsert should_insert;
	NonLeafNode nonleafNode;
	int i;

	ph = MAKE_PAGE_HANDLE();

	pinPage(btree_mgmtData->bp,ph,node);
	nh = (NodeHeader *)ph->data;

	if (nh->isLeafNode) {
		should_insert = place_key_rid(nh, btree_mgmtData, key,rid);
		unpinPage(btree_mgmtData->bp,ph);
		free(ph);
		return should_insert;
	}

	nonleafNode = (NonLeafNode)(nh+1);
	//Can use binary search to optimize.
	for (i = 1; i < nh->numKeys; ++i) {
		if (key->v.intV < nonleafNode[i].key.v.intV) {
			break;
		}
	}

	should_insert = _insert(mgmtData,key,rid,nonleafNode[i-1].pointer);
	if (should_insert.should_insert == TRUE) {
		should_insert = place_key_pointer(nh,btree_mgmtData,should_insert.non_leaf_node_entry);
	}

	markDirty(btree_mgmtData->bp,ph);
	unpinPage(btree_mgmtData->bp,ph);
	free(ph);
	return should_insert;
}

/*
	Checks if space is available in the node
*/
static bool
SpaceAvailable(NodeHeader *nh,BTreeHeader *btreeHeader) 
{
	if (btreeHeader->fanOut == nh->numKeys) {
		return FALSE;
	}
	return TRUE;
}

/*
	Inserts rid in the leaf node
	Splits if space is not available
*/
static ShouldInsert
place_key_rid(NodeHeader *nh, BTreeMgmtData *btree_mgmtData, Value *key, RID rid)
{
	ShouldInsert should_insert;
	BTreeHeader *btreeHeader = &(btree_mgmtData->btreeHeader);
	if(SpaceAvailable(nh,btreeHeader) == TRUE) {
		should_insert.should_insert = FALSE;
		place_in_node(nh,btreeHeader,key,rid);	
	}else {
		should_insert.should_insert = TRUE;
		should_insert.non_leaf_node_entry = split_leaf_node(nh,btree_mgmtData,key,rid);
	}
	return should_insert;
}

/*
	Inserts key & pointer in non-leaf node.
	Splits Non leaf node if space is not available
*/
static ShouldInsert
place_key_pointer(NodeHeader *nh, BTreeMgmtData *btree_mgmtData, NonLeafNodeEntry new_entry)
{
	ShouldInsert should_insert;
	BTreeHeader *btreeHeader = &(btree_mgmtData->btreeHeader);
	if(SpaceAvailableInNonLeafNode(nh,btreeHeader) == TRUE) {
		should_insert.should_insert = FALSE;
		place_in_non_leaf_node(nh,btreeHeader,new_entry,OLD_NODE);	
	}else {
		should_insert.should_insert = TRUE;
		should_insert.non_leaf_node_entry = split_non_leaf_node(nh,btree_mgmtData,new_entry);
	}
	return should_insert;
}


/*
	Insert rid in leaf node
*/
static void
place_in_node(NodeHeader *nh, BTreeHeader *btreeHeader, Value *key, RID rid)
{
	LeafNode leafNode = (LeafNode)(nh+1);
	uint64_t numKeys = nh->numKeys;
	int i = 0;

	for (i = 0; i < numKeys; ++i) {
		if (key->v.intV < leafNode[i].key.v.intV) {
			break;
		}
	}

	if (i != numKeys) {
		memmove(&leafNode[i+1],&leafNode[i],sizeof(LeafNodeEntry)*(nh->numKeys-i));		
	}

	++nh->numKeys;
	++btreeHeader->numEntries;
	leafNode[i].key = *key;
	leafNode[i].rid = rid;
}

/*
	Splits leaf node
*/
static NonLeafNodeEntry
split_leaf_node(NodeHeader *nh, BTreeMgmtData *btree_mgmtData, Value *key, RID rid)
{
	NonLeafNodeEntry nonLeafNodeEntry;
	BTreeHeader *btreeHeader = &(btree_mgmtData->btreeHeader);
	BM_BufferPool *bp = btree_mgmtData->bp;
	BM_PageHandle *ph = NULL;
	uint64_t split_index;
	assert(btreeHeader->fanOut == nh->numKeys);

	NodeHeader *tmpnh = nh;

	NodeHeader *newnh = NULL;
	LeafNode leafNode = (LeafNode)(nh+1);
	LeafNode newleafNode;
	Value split_element_key;

	//Allocate new node
	uint64_t newNode = AllocateNode(btree_mgmtData,LEAF_NODE);

	ph = MAKE_PAGE_HANDLE();
	
	pinPage(bp,ph,newNode);

	newnh = (NodeHeader *) ph->data;
	newleafNode = (LeafNode)(newnh+1);

	split_index = nh->numKeys / 2;
	split_element_key = leafNode[split_index].key;

	if (key->v.intV > split_element_key.v.intV) {
		++split_index;
		tmpnh = newnh;
	}

	//Divide the keys
	memcpy(&newleafNode[0],&leafNode[split_index],sizeof(LeafNodeEntry)*(nh->numKeys-split_index));
	
	newnh->numKeys = nh->numKeys-split_index;

	nh->numKeys = split_index;

	//place the key in node
	place_in_node(tmpnh, btreeHeader, key, rid);

	newnh->nextNode = nh->nextNode;
	newnh->prevNode = nh->pageNum;
//	nh->nextNode->prevNode = newNode;
	nh->nextNode = newNode;	

	nonLeafNodeEntry.key = newleafNode[0].key;
	nonLeafNodeEntry.pointer = newNode;

	markDirty(bp,ph);
	unpinPage(bp,ph);
	free(ph);
	return nonLeafNodeEntry;
}

/*
	Checks if space available in non-leaf node
*/
static bool
SpaceAvailableInNonLeafNode(NodeHeader *nh,BTreeHeader *btreeHeader) 
{
	assert(nh->isLeafNode == FALSE);
	//Other possible solution make union of keys and pointers in nodeheader struct
	uint64_t pointers = nh->numKeys;
	uint64_t numKeys = pointers - 1;

	if (btreeHeader->fanOut == numKeys) {
		return FALSE;
	}
	return TRUE;
}

/*
	Insert pointer in non leaf node
*/
static void
place_in_non_leaf_node(NodeHeader *nh, BTreeHeader *btreeHeader, NonLeafNodeEntry non_leaf_node_entry, int new_split_node)
{
	NonLeafNode nonleafNode = (NonLeafNode)(nh+1);
	uint64_t numKeys = nh->numKeys;
	int i = 0;

	for (i = new_split_node; i < numKeys; ++i) {
		if (non_leaf_node_entry.key.v.intV < nonleafNode[i].key.v.intV) {
			break;
		}
	}

	if (i != numKeys) {
		memmove(&nonleafNode[i+1],&nonleafNode[i],sizeof(NonLeafNodeEntry)*(nh->numKeys-i));		
	}

	++nh->numKeys;
	nonleafNode[i] = non_leaf_node_entry;
}

/*
	Splits non-leaf node
	Returns pointer to previous level
*/
static NonLeafNodeEntry
split_non_leaf_node(NodeHeader *nh, BTreeMgmtData *btree_mgmtData, NonLeafNodeEntry new_entry)
{
	
	//return node
	NonLeafNodeEntry nonLeafNodeEntry;
	BTreeHeader *btreeHeader = &(btree_mgmtData->btreeHeader);
	BM_BufferPool *bp = btree_mgmtData->bp;
	BM_PageHandle ph;

	NonLeafNode nonleafNode = (NonLeafNode)(nh+1);

	NodeHeader *nhtoadd = nh;
	NodeHeader *newnh = NULL;
	NonLeafNode newNonleafNode;
	
	Value split_element_key; //since datatype value not nonleafnode so that we can change datatype later for comparison
	Value key;
	uint64_t split_index;

	uint64_t newNode = AllocateNode(btree_mgmtData,NON_LEAF_NODE);

	pinPage(bp,&ph,newNode);

	newnh = (NodeHeader *) ph.data;
	newNonleafNode = (NonLeafNode)(newnh+1);

	split_index = nh->numKeys / 2;
	split_element_key = nonleafNode[split_index].key;
	key = new_entry.key;

	if (key.v.intV > split_element_key.v.intV) {
		++split_index;
		nhtoadd = newnh;
	}

	memcpy(&newNonleafNode[0],&nonleafNode[split_index],sizeof(NonLeafNodeEntry)*(nh->numKeys-split_index));
	
	newnh->numKeys = nh->numKeys-split_index;
	nh->numKeys = split_index;

	place_in_non_leaf_node(nhtoadd, btreeHeader, new_entry, NEW_SPLIT_NODE);

//	newnh->nextNode = nh->nextNode;
	newnh->prevNode = nh->pageNum;
//	nh->nextNode->prevNode = newNode;
	nh->nextNode = newNode;	


	nonLeafNodeEntry.key = newNonleafNode[0].key;
	nonLeafNodeEntry.pointer = newNode;


	markDirty(bp,&ph);
	unpinPage(bp,&ph);
	
	return nonLeafNodeEntry;
}

void 
_deletekey(BTreeHeader *btreeHeader,Value *key)
{
 	return;
}

/*
	Finds key in leaf node
*/
RID
findKeyInLeafNode(NodeHeader *nh, Value *key)
{
	int i;
	RID rid = {-1,-1};
	LeafNode leafNode = (LeafNode)(nh+1);
	for (i =0 ; i < nh->numKeys; ++i) {
		if (leafNode[i].key.v.intV == key->v.intV) {
			break;
		}
	}	
	rid = leafNode[i].rid;
	return rid;
}

/*
	Finds pointer to non leaf node which is greater than or equal to the key
*/
uint64_t
findNearestKey(NodeHeader *nh, Value *key)
{
	int i;
	NonLeafNode nonleafNode = (NonLeafNode)(nh+1);
	for (i =1 ; i < nh->numKeys; ++i) {
		if (key->v.intV < nonleafNode[i].key.v.intV) {
			return nonleafNode[i-1].pointer;
		}
	}	
	return nonleafNode[i-1].pointer;
}

/*
	Gets leftmost leaf node
*/
uint64_t
getLeafNode(BTreeMgmtData *mgmtData)
{
	BM_BufferPool *bp = mgmtData->bp;
	BTreeHeader *btreeHeader = &mgmtData->btreeHeader;
	BM_PageHandle ph;

	uint64_t node = btreeHeader->root;
	NodeHeader *nh = NULL;

	NonLeafNode nonleafNode;

	do{
		pinPage(bp,&ph,node);
		nh = (NodeHeader *)ph.data;
		if (nh->isLeafNode) {
			unpinPage(bp,&ph);
			return node;
		}
		nonleafNode = (NonLeafNode)(nh+1);
		node = nonleafNode[0].pointer;
		unpinPage(bp,&ph);
	}while(TRUE);

	return 0;
}

/*
	Allocates IndexScanHandleMgmtData
*/
IndexScanHandleMgmtData *
CreateIndexScanMgmtData(void *mgmtData)
{
	BTreeMgmtData *btree_mgmtData = mgmtData;
    IndexScanHandleMgmtData *scmgmtData = (IndexScanHandleMgmtData *)malloc(sizeof(IndexScanHandleMgmtData));
	scmgmtData->leafNode = getLeafNode(btree_mgmtData);
	scmgmtData->key_position = 0;
	return scmgmtData;
}
