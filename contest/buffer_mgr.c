#include <stdlib.h>
#include <limits.h>
#include <strings.h>
#include <string.h>
#include "dberror.h" // Include return codes and methods for logging errors
#include "dt.h" // Include bool DT

#include "storage_mgr.h" 
#include "buffer_mgr.h"
#include "page.h"

#include "list.h"
#include "pagehash.h"

#define MIN_CLOCK_LOOP 3
/************************************************************
 *                    Data structures                       *
 ************************************************************/
typedef struct BufferPoolMgmtData {
    PageHead *pageHeads; /* PageHeaders */
    char (*pageFrames)[PAGE_SIZE]; /* Block of memory to store data */
    unsigned int allocationIndex; /* Index to track allocated pages in buffer pool */
    unsigned int evictIndex;
    PageNumber *frames; /* Data Structure to Store Page numbers at ith page frame */
    bool *dirty; /* Data Structure to store status of pages where status represents whether the page is dirty or not*/
    int *fixedCount;
    int nRead;  /* Count of number of reads from disk */
    int nWrite; /* Count of number of writes to disk */
    ListNode listHead; /* Head node of the list */
    HashTable htable; /* Hashtable for optimizing search for a page */
} BufferPoolMgmtData;

PageHead * search(BM_BufferPool * bm, PageNumber pageNum);
PageHead * allocate(BM_BufferPool * bm);
PageHead * evict(BM_BufferPool * bm);
PageHead * searchLessFrequentPage(BM_BufferPool * bm);
PageHead * searchClockPage(BM_BufferPool * bm);

RC readPage(BM_BufferPool * bm, PageHead * ph);
RC writePage(BM_BufferPool * bm, PageHead * ph);

void evictAlgoPostPin(BM_BufferPool * bm, PageHead * ph);
void evictAlgoPostUnpin(BM_BufferPool * bm, PageHead * ph);

/*
    Intialize and Create Buffer Pool Management Data
*/
void createBufferPoolMgmtData(BM_BufferPool * bm)
{
    int i;
    BufferPoolMgmtData *mgmtData;

    /* Allocates buffer pool management data */
    mgmtData = bm->mgmtData =(BufferPoolMgmtData *)malloc(sizeof(BufferPoolMgmtData));
    /* Clear buffer pool management data*/
    bzero(mgmtData,sizeof(BufferPoolMgmtData));

    mgmtData->pageFrames = malloc(bm->numPages * PAGE_SIZE);
    mgmtData->pageHeads = malloc(sizeof(PageHead) * bm->numPages);

    memset(mgmtData->pageFrames, 0, bm->numPages * PAGE_SIZE);
    bzero(mgmtData->pageHeads,sizeof(PageHead) * bm->numPages);

    mgmtData->frames = malloc(sizeof(PageNumber) * bm->numPages);
    mgmtData->dirty = malloc(sizeof(bool) * bm->numPages);
    mgmtData->fixedCount = malloc(sizeof(unsigned int) * bm->numPages);
    
    mgmtData->allocationIndex = 0;
    mgmtData->evictIndex = 0;

    

    /* Initialize the list head */
    InitListNode(&mgmtData->listHead);
    
    /* Initialize the page headers */
    for (i = 0; i < bm->numPages; ++i) {

        /* Initialize Page Numbers */
        mgmtData->pageHeads[i].pageNum = NO_PAGE;
        mgmtData->frames[i] = NO_PAGE;

        /* Initialize data pointers to NULL */
        mgmtData->pageHeads[i].data = NULL;

        /* Clear dirty flags */
        mgmtData->pageHeads[i].dirty = false;        
        mgmtData->dirty[i] = false;

        mgmtData->pageHeads[i].fixedCount = 0;
        mgmtData->fixedCount[i] = 0;

        mgmtData->pageHeads[i].frequency = 0;
        mgmtData->pageHeads[i].index = i;

        /* Initialize head pointers */
        InitListNode(&mgmtData->pageHeads[i].listLink);
        InitListNode(&mgmtData->pageHeads[i].listHash);

        mgmtData->pageHeads[i].used = false;
    }

    /*
        Initialize the hash table
    */
    InitHashTable(mgmtData->htable);
}

/*
    De-Intialize and Destroy Buffer Pool Management Data
*/
void destroyBufferPoolMgmtData(BM_BufferPool *const bm)
{
    int i;
    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    
    int num_pinned_pages = 0;
    for (i = 0; i < bm->numPages; ++i) {
        //ASSERT(mgmtData->dirty == 0);        
        if (mgmtData->pageHeads[i].fixedCount != 0) {
            ++num_pinned_pages;
        }
        RemoveFromList(&mgmtData->pageHeads[i].listLink);

        mgmtData->frames[i] = NO_PAGE;
        mgmtData->dirty[i] = false;
        mgmtData->fixedCount[i] = 0;
        
    }

    InitHashTable(mgmtData->htable);

    mgmtData->evictIndex = 0;
    mgmtData->allocationIndex = 0;

    
    /*
        Free Allocated Memory for Data Structures required by Statistical Functions
    */
    free(mgmtData->frames);
    free(mgmtData->dirty);
    free(mgmtData->fixedCount);

    free(mgmtData->pageHeads);
    free(mgmtData->pageFrames);
    
    free(mgmtData);    
}


/************************************************************
 *               Buffer Pool Functions                      *
 ************************************************************/

 /*
    Creates a new buffer pool with numPages page frames 
        using the page replacement strategy strategy. 
    The pool is used to cache pages from the page file with name pageFileName.
 */
RC initBufferPool(BM_BufferPool *const bm, const char *const pageFileName, 
          const int numPages, ReplacementStrategy strategy, 
          void *stratData)
{
    int status;
    
    // Open page file
    SM_FileHandle fh;
    status = openPageFile((char *)pageFileName, &fh);
    if (status != RC_OK)
    {
        return status;
    }

    // Fill in bm
    bm->pageFile=(char *)pageFileName;
    bm->numPages=numPages;
    bm->strategy=strategy;
    createBufferPoolMgmtData(bm);
    
    closePageFile(&fh);
    return RC_OK;
}

/*
    Flush all the pages
    Clear BufferPool
*/
RC shutdownBufferPool(BM_BufferPool *const bm)
{
    int status;
    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    status = forceFlushPool(bm);
    if (status !=RC_OK)
    {
        return status;
    }
    // Reset values in bm and copy file pointer in tmp
    
    bm->pageFile = "";
    bm->numPages = 0;
    bm->strategy = 0;
    
    destroyBufferPoolMgmtData(bm);

    return RC_OK;
}

/*
    Flush Buffer Pool (Cache) to disk 
     -Causes all dirty pages (with fix count 0) from the buffer pool to be written to disk.
    Clears the dirty bits
*/
RC forceFlushPool(BM_BufferPool *const bm){
    char *pageFile;
    int status;
    SM_FileHandle fh;
    int i;
    BufferPoolMgmtData *mgmtData = (BufferPoolMgmtData *) bm->mgmtData;
   
    
   
    pageFile = bm->pageFile;
    status = openPageFile(pageFile,&fh);
    if (status != RC_OK)
    {
        
        return status;
    }

    /* Traverse all allocated pages */
    for (i = 0; i < mgmtData->allocationIndex; ++i){
        /* If the page is not dirty no need to flush
           Else flush page i.e write page data to disk
        */
        
        if (mgmtData->pageHeads[i].dirty == true) {
            status = ensureCapacity(mgmtData->pageHeads[i].pageNum,&fh);
            if (status != RC_OK)
            {
                
                return status;
            }

            status = writeBlock(mgmtData->pageHeads[i].pageNum,&fh,mgmtData->pageHeads[i].data);
            if (status != RC_OK)
            {
                
                return status;
            }

            mgmtData->nWrite+=1;
            mgmtData->pageHeads[i].dirty = false;
            mgmtData->dirty[i] = false;
            
        }

    }

    
    closePageFile(&fh);
    return status;
}

/************************************************************
 *               Page Management Functions                  *
 ************************************************************/

/*
    Marks Page Dirty
*/
RC markDirty (BM_BufferPool *const bm, BM_PageHandle *const page){
    PageHead *ph;
    BufferPoolMgmtData *mgmtData = (BufferPoolMgmtData *) bm->mgmtData;
    
    ph = search(bm,page->pageNum);
    if (ph==NULL)
    {
        
        return RC_NON_EXISTING_PAGE;
    }
    
    ph->dirty=true;
    mgmtData->dirty[ph->index] = ph->dirty;
    
    return RC_OK;
}

/*
    Flush the page and hence the clears the dirty bit.
*/
RC forcePage (BM_BufferPool *const bm, BM_PageHandle *const page){
    int status;
    PageHead *ph;
    BufferPoolMgmtData *mgmtData = (BufferPoolMgmtData *) bm->mgmtData;
    
    /* Search for required page*/
    ph = search(bm,page->pageNum);
    /* write page to disk */
   
    status = writePage(bm,ph);
    
    return status;
}

/*
    Pins the page
    Algo:
        search for page
        if not avaliable then allocate a page
        if cannot allocate(since the buffer pool is full) then evict the page
    Updates PageHandle
*/
RC pinPage (BM_BufferPool *const bm, BM_PageHandle *const page, 
        const PageNumber pageNum){
    PageHead *ph;
    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    

    /* Search for page with page number pageNum */
    ph = search(bm,pageNum);

    if (ph==NULL)
    {
        /* Allocates a empty page */
        ph = allocate(bm);
        if (ph==NULL)
        {
            /*
                If cannot allocate find page to evict 
             */
            ph = evict(bm);
            if (ph == NULL)
            {
                /*
                    If cannot find page to evict then returns pinned failed 
                */
                
                return RC_PINNED_FAILED;
            }
        }
        /*
            Remove page from hash table since the pageNum will change 
        */
        RemovePage(mgmtData->htable,ph);
        /*
            Update page number 
        */
        ph->pageNum = pageNum;
        /*
            Read the required page from disk 
        */
        
        readPage(bm,ph);
    }

    /*
        Update fix count
    */
    mgmtData->fixedCount[ph->index] +=1;
    ph->fixedCount +=1;

    evictAlgoPostPin(bm, ph);

    /*
        Update Page Handle
    */
    page->pageNum = pageNum;
    page->data = (ph->data);
    
    return RC_OK;
}

/*
    Unpins the Page
    The pageNum field of PageHandle page is used to figure out which page to unpin.
*/
RC unpinPage (BM_BufferPool *const bm, BM_PageHandle *const page){
    PageHead *ph;
    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    
    /*
        Search for the required page
    */
    ph = search(bm,page->pageNum);
    if (ph == NULL)
    { /* Page not found  */
        
        return RC_NON_EXISTING_PAGE;
    }

    /*
        Ensures the fix count doesn't go below zero
    */
    if (ph->fixedCount>0)
    {
        mgmtData->fixedCount[ph->index] -= 1;
        ph->fixedCount -= 1;
    }

    evictAlgoPostUnpin(bm, ph);

    
    return RC_OK;
}

/************************************************************
 *                    Statistics Functions                  *
 ************************************************************/

/* 
    Returns an array of PageNumbers (of size numPages) where the ith element is the number of the page stored in the ith page frame. 
    An empty page frame is represented using the constant NO_PAGE.
*/
PageNumber *getFrameContents (BM_BufferPool *const bm){
    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    PageNumber *frames;
    
    frames = mgmtData->frames;
    
    return frames;
}

/*
    Returns an array of bools (of size numPages) 
    where the ith element is TRUE if the page stored in the ith page frame is dirty.
*/
bool *getDirtyFlags (BM_BufferPool *const bm){
    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    bool *dirty;
    
    dirty = mgmtData->dirty;
    
    return dirty;
}

/*
    Returns an array of ints (of size numPages) where the ith element is the fix count of the page stored in the ith page frame. 
    Return 0 for empty page frames.
*/
int *getFixCounts (BM_BufferPool *const bm){
    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    int *fixedCount;
    
    fixedCount = mgmtData->fixedCount;
    
    return fixedCount;
}

/*
    Returns the number of pages that have been read from disk 
        since a buffer pool has been initialized. 
*/
int getNumReadIO (BM_BufferPool *const bm){
    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    int nRead;
    
    nRead = mgmtData->nRead;
    
    return nRead;
}

/*
    Returns the number of pages that have been written to disk 
        since a buffer pool has been initialized. 
*/
int getNumWriteIO (BM_BufferPool *const bm){
    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    int nWrite;
    
    nWrite = mgmtData->nWrite;
    
    return nWrite;
}

/************************************************************
 *               Utility Functions for Page Management      *
 ************************************************************/

/*
    Search page in Cache
    Optimized: Implemented and Used hash table for searching the page
*/
PageHead * search(BM_BufferPool * bm, PageNumber pageNum)
{
    PageHead * ph = NULL;
    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    /* Search pagehead for page number 'pageNum' using hash table */
    ph = SearchPage(mgmtData->htable,pageNum);
    return ph;
}

/*
    Search for less frequent page in cache and zero fixcount
*/
PageHead * searchLessFrequentPage(BM_BufferPool * bm)
{
    PageHead * ph = NULL;
    int i;
    unsigned long minfrequency = ULONG_MAX;
    BufferPoolMgmtData *mgmtData = bm->mgmtData;

    /*
        Traverse all allocated pages to find less frequent evictable page
    */
    for (i = 0; i < mgmtData->allocationIndex; ++i){
        if (mgmtData->pageHeads[i].frequency<minfrequency && mgmtData->pageHeads[i].fixedCount==0) {
            ph = &mgmtData->pageHeads[i];
            minfrequency = mgmtData->pageHeads[i].frequency;
        }
    }
    return ph;
}

PageHead * searchClockPage(BM_BufferPool * bm)
{
    PageHead * ph = NULL;
    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    int i = mgmtData->evictIndex;
    int j;
    int found=false;
    for (j = 0; j < MIN_CLOCK_LOOP; ++j)
    {
        do {
            if (mgmtData->pageHeads[i].fixedCount == 0) {
                ph = &mgmtData->pageHeads[i];
                if (ph->used==false)
                {
                    j = MIN_CLOCK_LOOP;
                    found = true;
                    break;
                }else{
                    ph->used=false;
                }
            }
            i = (i + 1) % mgmtData->allocationIndex;
        } while (i != mgmtData->evictIndex);
    }
    if (mgmtData->evictIndex == i && found==false)
    {
        ph=NULL;
    }

    mgmtData->evictIndex = (i+1) % mgmtData->allocationIndex;
    return ph;
}



/*
    Allocates an empty page in cache
    Once Cache is full always returns NULL  
*/
PageHead * allocate(BM_BufferPool * bm)
{

    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    PageHead *ph;
    /* If there is no free page to allocate then returns NULL*/
    if (mgmtData->allocationIndex >= bm->numPages) {
        return NULL;
    }
    ph  = &mgmtData->pageHeads[mgmtData->allocationIndex];
    ph->data = mgmtData->pageFrames[mgmtData->allocationIndex];
    mgmtData->allocationIndex++;
    return ph;
}

/*
    Perform post pin action as per current eviction algorithm. 
*/
void evictAlgoPostPin(BM_BufferPool * bm, PageHead * ph)
{
    /*
        If strategy is LRU and fix count is one then remove page from list
        since page cannot be evicted.  
    */
    if (bm->strategy == RS_LRU && ph->fixedCount == 1)
    {
          RemoveFromList(&(ph->listLink));
    }

    /*
        If strategy is LFU then increase frequency
    */
    if (bm->strategy == RS_LFU)
    {
         ph->frequency +=1;
    }
}

/*
    Perform post unpin action as per current eviction algorithm. 
*/
void evictAlgoPostUnpin(BM_BufferPool * bm, PageHead * ph)
{
    BufferPoolMgmtData *mgmtData = bm->mgmtData;

    /*
        If strategy is LRU and fix count is zero then add to list since page is
        now eligible for eviction
        Note: This optimizes search for free page in evict function
    */
    if (ph->fixedCount == 0)
    {
        ph->used = true;
        if (bm->strategy == RS_LRU)
        {
            AddToBack(&mgmtData->listHead,&ph->listLink);    
        }
    }
}

/*
    Search for page according to the Replacement Strategy and zero fixcount 
*/
PageHead * evict(BM_BufferPool * bm)
{
    PageHead * ph = NULL;
    BufferPoolMgmtData *mgmtData = bm->mgmtData;
    ListNode * node;
    if (bm->strategy==RS_LFU)
    {
        /*
            Search for less frequent evictable page        
        */
        ph = searchLessFrequentPage(bm);
    }else if (bm->strategy==RS_CLOCK)
    {
        ph = searchClockPage(bm);
    }else{
        /*
            For FIFO or LRU traverse to list to find first evictable page       
        */
        for (node = mgmtData->listHead.next; node != &(mgmtData->listHead); node = node->next){
            ph = GetEnclosingTypePtr(PageHead,listLink, node);
            if (ph->fixedCount == 0) {
                break;
            }
        }
        if (ph != NULL && ph->fixedCount == 0) {
            RemoveFromList(&(ph->listLink));
        }
    }
    if (ph == NULL || ph->dirty == false || ph->fixedCount != 0) {
        return ph;
    }

    
    writePage(bm, ph);
    
    return ph;
}


/*
    Reads page from pagefile using storage manager
*/
RC readPage(BM_BufferPool * bm, PageHead * ph)
{
    int status;
    BufferPoolMgmtData *mgmtData = (BufferPoolMgmtData *) bm->mgmtData;
    SM_FileHandle fh;
    char *pageFile;


    pageFile = bm->pageFile;
    status = openPageFile(pageFile,&fh);
    if (status != RC_OK)
    {
        return status;
    }

    status = ensureCapacity(ph->pageNum,&fh);
    if (status != RC_OK)
    {
        return status;
    }

    status = readBlock(ph->pageNum,&fh,ph->data);
    if (status != RC_OK)
    {
        return status;
    }

    if (bm->strategy==RS_FIFO)
    {
        AddToBack(&mgmtData->listHead,&ph->listLink);
    }

    if (bm->strategy==RS_LFU)
    {
            ph->frequency=0;
    }

    InsertPage(mgmtData->htable,ph);
    ph->dirty = false;

    mgmtData->dirty[ph->index] = ph->dirty;
    mgmtData->frames[ph->index] = ph->pageNum;
    mgmtData->nRead+=1;

    closePageFile(&fh);
    return RC_OK;
}

/*
    Writes page to pagefile using storage manager
*/
RC writePage(BM_BufferPool * bm, PageHead * ph)
{
    int status;
    BufferPoolMgmtData *mgmtData = (BufferPoolMgmtData *) bm->mgmtData;
    SM_FileHandle fh;
    char *pageFile;

    pageFile = bm->pageFile;
    status = openPageFile(pageFile,&fh);
    if (status != RC_OK)
    {
        return status;
    }

    status = ensureCapacity(ph->pageNum,&fh);
    if (status != RC_OK)
    {
        return status;
    }

    // Write to file, update ph stats
    status = writeBlock(ph->pageNum,&fh,ph->data);
    if (status != RC_OK)
    {
        return status;
    }

    mgmtData->nWrite+=1;
    ph->dirty = false;
    mgmtData->dirty[ph->index] = ph->dirty;
    closePageFile(&fh);
    return RC_OK;
}