﻿ReadME




Objective


The objective of this assignment is to build a B+-Tree index.
Build & Run


To compile the code, run the following command:


> make


Next, run the following command


> ./test_assign4


This will run test code for buffer manager.


To revert the compilation, run following command:
> make clean


________________
Valgrind Results


==5545==
==5545== HEAP SUMMARY:
==5545==     in use at exit: 0 bytes in 0 blocks
==5545==   total heap usage: 5,948 allocs, 5,948 frees, 446,232,976 bytes allocated
==5545==
==5545== All heap blocks were freed -- no leaks are possible
==5545==
==5545== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==5545== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)


Introduction


This assignment implements B+-tree index. The index is backed up by a page file and pages of the index is accessed through buffer manager which is implemented in assignment 2. For simplicity each node occupies one page. A B+-tree stores pointer to records (the RID used by record manager to identify records) index by a keys of a given datatype. However, this implementation only supports int datatypes.  Pointers to intermediate nodes is represented by the page number of the page the node is stored in.  This fan-out of B+-tree supported by this implementation is limited. 




________________
Design
Data Structures
1. BTreeHeader


BTree Header stores metadata for B+-tree. Every B+-tree have one BtreeHeader. It stores following fields:
        
uint64_t root;
        uint64_t numNodes;
        uint64_t numEntries;
        uint64_t fanOut;


root stores pointer to root. The pointer is page number of the page representing root node.
numNodes stores total number of nodes in the B+-tree. 
numEntries stores total number of entries in B+-tree. It is updated after every insert or deletion of the key.
fanOut is fan out of the B+tree. It is initialized when B+tree is created. 


1. BTreeMgmtData


It is in-memory data-structure which stores management data for B+-trees. It stores following fields:


BTreeHeader btreeHeader;
        BM_BufferPool *bp;
        
btreeHeader is datastructure which stores metadata information for B+-tree. It is initialized once B+-tree is loaded into memory. It is flushed back to disk after closing B+-tree.
bp is pointer to buffer pool which is associated with each B+-tree. Pages for B+-tree are accessed through this buffer pool.


1. NodeHeader
Node Header stores metadata information for that node.  Every node(page) has NodeHeader. It stores following fields:


        bool isLeafNode;
        int pageNum;
        uint64_t numKeys;
        uint64_t nextNode;
        uint64_t prevNode;
        uint64_t parentNode; 


isLeafNode stores whether the node is leaf node or non-leaf node.
pageNum stores the page number associated with that page.
numKeys stores information about total number of keys stored in that node.
nextNode stores page number of next node.
prevNode stores page number of prev node.
parentNode stores page number of parent node.


1. LeafNodeEntry
LeafNodeEntry is representation of entries in leaf node. Leaf node entries are stored in this format on disk. 
        Value key;
        RID rid; 
key is key for the entry.
rid is pointer to record. 




________________


1. NonLeafNodeEntry
NonLeafNodeEntry is representation of entries in non-leaf node. Non-leaf node entries are stored in this format on disk. 
                Value key;
                uint64_t pointer;
key is key for non-leaf node entry.
pointer is page number for the child node having keys greater or equal to ‘key’
________________
Implementation


RC getNumNodes (BTreeHandle *tree, int *result);
RC getNumEntries (BTreeHandle *tree, int *result);
        Above functions access static information like number of nodes and number of entries stored in that B+tree from B+-tree header.


RC getKeyType (BTreeHandle *tree, DataType *result);
        Return datatype of key from BtreeHandle.


RC createBtree (char *idxId, DataType keyType, int n);
RC openBtree (BTreeHandle **tree, char *idxId);
RC closeBtree (BTreeHandle *tree);
RC deleteBtree (char *idxId);
        Above functions creates, opens, closes and deletes B+-tree index. The deleteBtree removes the corresponding page file and de-allocates the data structure allocated with that B+-tree . Furthermore, before a client can access an b-tree index all functions ensures that B+-tree was opened (openBtree). When closing a b-tree (closeBtree), the index manager ensures that all new or modified pages of the index are flushed back to disk (uses the buffer manager forceFlushPool function for that).


RC findKey (BTreeHandle *tree, Value *key, RID *result);
RC insertKey (BTreeHandle *tree, Value *key, RID rid);
RC deleteKey (BTreeHandle *tree, Value *key);
RC openTreeScan (BTreeHandle *tree, BT_ScanHandle **handle);
RC nextEntry (BT_ScanHandle *handle, RID *result);
RC closeTreeScan (BT_ScanHandle *handle);


These functions are used to find, insert, and delete keys in/from a given B+-tree. findKey return the RID for the entry with the search key in the b-tree. If the key does not exist this function returns RC_IM_KEY_NOT_FOUND. 
insertKey inserts a new key and record pointer pair into the index. It returns error code RC_IM_KEY_ALREADY_EXISTS if this key is already stored in the b-tree. 


deleteKey removes a key (and corresponding record pointer) from the index. It should return RC_IM_KEY_NOT_FOUND if the key is not in the index. For deletion it is up to the client whether this is handled as an error.


Furthermore, clients can scan through all entries of a BTree in sort order using the openTreeScan, nextEntry, and closeTreeScan methods. 
The nextEntry method returns RC_IM_NO_MORE_ENTRIES if there are no more entries to be returned (the scan has gone beyond the last entry of the B+-tree.