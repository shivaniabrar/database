#include "dt.h"
#include "list.h"

#ifndef HASH_H
#define HASH_H

#define HASH_TABLE_SIZE 100

/************************************************************
 *                    Data structures                       *
 ************************************************************/

typedef ListNode HashTable[HASH_TABLE_SIZE];
typedef ListNode * Value;
typedef int Key;
typedef bool (*SearchFunc)(Key,Value);

/************************************************************
 *                    Interface                             *
 ************************************************************/
void InitHashTable(HashTable htable);
void Insert(HashTable htable,Key key,Value value);
Value Search(HashTable htable,Key key,SearchFunc search);
void Remove(HashTable htable,Value value);

/************************************************************
 *                    Utility                               *
 ************************************************************/
int hash(Key key);

#endif
