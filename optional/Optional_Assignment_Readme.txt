﻿ReadME




Objective


The objective of this assignment is to implement a standard operator algorithm on top of record manager




Build & Run


To compile the code, run the following command:


> make


Next, run the following command


> ./test_assign3


This will run test code for buffer manager.


To revert the compilation, run following command:
> make clean




Valgrind Results


==2201== HEAP SUMMARY:
==2201==     in use at exit: 0 bytes in 0 blocks
==2201==   total heap usage: 521,285 allocs, 521,285 frees, 755,349,799 bytes allocated
==2201==
==2201== All heap blocks were freed -- no leaks are possible
==2201==
==2201== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
==2201== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)


Introduction
This assignment implements a standard operator algorithm on top of record manager. The record manager handles tables with a fixed schema. Clients can insert records, delete records, update records, and scan through the records in a table. A scan is associated with a search condition and only returns records that match the search condition. It also supports conditional updating of record and null values.
________________
Algorithms:
This assignment implements following algorithms on top of record manager.
Nested-Loop-Join Algorithm:
        The algorithm joins two relations. This algorithm is as follows :-


1. Create new table 
2. Schema for new table is amalgamation of schema of table1 and table 2.
3. for record in table1:
   1. for record in table2:
      1.  new_record =  concat(table1_record, table2_record)
      2.  insert(new_table, new_record)




Union Algorithm:
The algorithm combines the results of two SQL queries into a single table of all matching rows. The two queries must result in the same number of columns and compatible data types in order to unite. 


1. Creates new table
2. Schema for new table is same for table1 or table2
3. for record in table1:
   1. insert(new_table, table1_record);
1. for record in table2:
   1. insert(new_table, table1_record);




________________


Design
Data Structures
1. Page Header
Page Header stores metadata for page. It is of fixed size and is stored in the start of the page. It stores following fields:
        uint64_t next_free_page;
        uint64_t free_slot_start;
        uint64_t padding[PAGE_HEADER_PADDING];
next_free_page stores page number of free page. All pages having free space are linked using this field. 
free_slot_start stores slot number of free slots. All free slots in a page are linked using this field. 
Above two fields are used to allocate and free records. 


1. Record Header
Record Header stores metadata information of the record. Each slot has record header and record. It stores following fields:
                 char allocated : 1;
                char null[];
allocated is bit field which is set when record is allocated and unset when it not allocated. This field help to implement tombstone. 
null is an array of bit fields which unsets for an attribute in record it is not null.


________________




1. Table Header
Table Header stores metadata information of the table.  It stores following fields:
        uint64_t slots_in_page;
        uint64_t free_page_start;
        uint64_t num_tuples;
        uint64_t recordSize;
        uint64_t recordHeaderSize;
        uint64_t num_pages;


slots_in_pages is maximum number of slots that can fit in a page.
free_page_start it stores number of page having free space. In a nutshell, it is head of free page list
num_tuples it stores number of tuples in the table. It changes when record is inserted or deleted.
record_size is the size of the record. This is calculated by using schema. Hence, every table will have different record size.
recordHeaderSize stores the size of the record header. This is calculated using schema since the schema is varying and hence array of bit fields storing null value information is need to be initialised
Conditional Update Scan (Optional Extension)
This implementation also supports conditional update scan. updateScan takes a condition (expression) which is used to determine which tuples to update and a pointer to a function which takes a record as input and returns the updated version of the record. That is the user of the updateScan method should implement a method that updates the record values and then pass this function to updateScan. 


test_assign3_1.c also includes test case to test conditional update scan. 


Encoding and Decoding
Transcoding module in the assignment supports encoding and decoding of a record and schema. The result of encoding of record and schema then can be used to store the schema and record in page. This module also defines encode and decode modules macros to encode and decode different datatypes.
Data types
Handling different datatypes is C is a tedious task. For instance, it makes code dirty since it inserts many if-else or switch cases in the code. Also, C doesn’t support templates. In this implementation, I have handled it using C++ style. This is designed by defining array of structure of pointers to functions for each datatype. And, finally mapping enum of datatypes to these functions. This made handling different datatypes a lot easier and also kept the code clean. This is implemented in datatypes.c