#include <string.h>
#include <stdlib.h>

#include "optionals.h"
#include "record_mgr_internals.h"
#include <assert.h>

static Schema * create_union_schema(RM_TableData *rel1, RM_TableData *rel2, int attr1, int attr2);
static int copy_records_with_given_attrs(RM_TableData *rel1, RM_TableData *rel2, int attr1, int attr2);
static Schema * create_join_schema(RM_TableData *rel1, RM_TableData *rel2);

static RC concat_records_and_insert(RM_TableData *rel, RM_TableData *rel1, RM_TableData *rel2);
static Schema *create_join_schema(RM_TableData *rel1, RM_TableData *rel2);
RC Nested_Loop_Join(RM_TableData *rel1, RM_TableData *rel2);

/*
	Finds union of rel1 and rel2
*/
RC
Union(RM_TableData *rel1, RM_TableData *rel2, int attr1, int attr2)
{

	RC status = RC_OK;
	char *infix = "_union_";
	RM_TableData rel;
	uint64_t size_of_table_name = strlen(rel1->name)+ strlen(infix) + strlen(rel2->name) + 1;
	char * table_name = malloc(sizeof(char) * size_of_table_name);
	

	//Creates table name
	strcpy(table_name,rel1->name);
	strcat(table_name,infix);
	strcat(table_name,rel2->name);

	TableMgmtData *rel_mgmtData = NULL;
	TableMgmtData *rel1_mgmtData = (TableMgmtData *)rel1->mgmtData;
	TableMgmtData *rel2_mgmtData = (TableMgmtData *)rel2->mgmtData;

	Schema *schema = create_union_schema(rel1, rel2, attr1, attr2);

	//Create & Open Table 
	createTable(table_name,schema);
	openTable (&rel, table_name);
	
	rel_mgmtData = (TableMgmtData *)rel.mgmtData;
	//Copy records to new table
	copy_records_with_given_attrs(&rel, rel1, 0, attr1);
	copy_records_with_given_attrs(&rel, rel2, 0, attr2);
	
	//Assert if numtuple in new table is consistent
	assert(rel_mgmtData->tableHeader.num_tuples 
		== (rel1_mgmtData->tableHeader.num_tuples + rel2_mgmtData->tableHeader.num_tuples));
	
	closeTable(&rel);
	freeSchema(schema);
	free(table_name);
	return status;
}

/*
	Creates schema for union of rel1 and rel2
*/
static Schema *
create_union_schema(RM_TableData *rel1, RM_TableData *rel2, int attr1, int attr2)
{
	Schema *schema;
	int numAttrs = 1;
	char **attrNames = malloc(sizeof(char*) * numAttrs);
	DataType *dataTypes = (DataType *) malloc(sizeof(DataType) * numAttrs);
	int *sizes = (int *) malloc(sizeof(int) * numAttrs);
  	int *keys = (int *) malloc(sizeof(int) * rel1->schema->keySize);

  	uint64_t attrNameLen = strlen(rel1->schema->attrNames[attr1])+1;

  	attrNames[0] = malloc(sizeof(char) * attrNameLen);
  	strcpy(attrNames[0],rel1->schema->attrNames[attr1]);
  	memcpy(&dataTypes[0], &rel1->schema->dataTypes[attr1], sizeof(DataType));
  	memcpy(&sizes[0], &rel1->schema->typeLength[attr1], sizeof(int));
	memcpy(&keys[0], &keys[0], sizeof(int));
	schema = createSchema (numAttrs, attrNames, dataTypes, sizes, rel1->schema->keySize, keys);

	return schema;
}

/*
	Copy record of records with given records
*/
static int
copy_records_with_given_attrs(RM_TableData *rel1, RM_TableData *rel2, int attr1, int attr2) 
{
	RC status = RC_OK;
	Record *record = NULL;
	Value *value = NULL;
	RID rid = {.page = -1,.slot = -1};
	do {
		//read records from r1
		createRecord (&record, rel2->schema);
		status = getNextRecord(rel2, &rid, record);
		if (status != RC_OK)
		{
			freeRecord(record);
			return status;
		}
		//get attribute from r1
		getAttr (record, rel2->schema, attr2, &value);
		freeRecord(record);
		//set attr to new record
		createRecord (&record, rel1->schema);
		setAttr (record, rel1->schema, attr1, value);
		//insert record to r3
		insertRecord (rel1, record);
		freeRecord(record);
		freeVal(value);
	}while(TRUE);
}

/*
	Joins two tables (cross join)
*/
RC
Nested_Loop_Join(RM_TableData *rel1, RM_TableData *rel2)
{
	RC status = RC_OK;
	char *infix = "_join_";
	RM_TableData rel;
	uint64_t size_of_table_name = strlen(rel1->name)+ strlen(infix) + strlen(rel2->name) + 1;
	char * table_name = malloc(sizeof(char) * size_of_table_name);
	
	strcpy(table_name,rel1->name);
	strcat(table_name,infix);
	strcat(table_name,rel2->name);

	TableMgmtData *rel_mgmtData = NULL;
	TableMgmtData *rel1_mgmtData = (TableMgmtData *)rel1->mgmtData;
	TableMgmtData *rel2_mgmtData = (TableMgmtData *)rel2->mgmtData;

	//Create schema for joining
	Schema *schema = create_join_schema(rel1, rel2);

	createTable(table_name,schema);
	openTable (&rel, table_name);
	
	rel_mgmtData = (TableMgmtData *)rel.mgmtData;

	//concat records of rel1 and rel2
	concat_records_and_insert(&rel, rel1, rel2);
	assert(rel_mgmtData->tableHeader.num_tuples 
		== (rel1_mgmtData->tableHeader.num_tuples * rel2_mgmtData->tableHeader.num_tuples));
	closeTable(&rel);
	freeSchema(schema);
	free(table_name);
	return status;
}

/*
	Creates schema for joining
*/
static Schema *
create_join_schema(RM_TableData *rel1, RM_TableData *rel2)
{
	Schema *rel1_schema = rel1->schema;
	Schema *rel2_schema = rel2->schema;
	Schema *schema = NULL;

	int attrs = 0;
	int i = 0;
	
	int numAttr = rel1_schema->numAttr + rel2_schema->numAttr;
	char **attrNames = malloc(sizeof(char*) * numAttr);
	DataType *dataTypes = (DataType *) malloc(sizeof(DataType) * numAttr);
	int *sizes = (int *) malloc(sizeof(int) * numAttr);
  	int *keys = (int *) malloc(sizeof(int) * (rel1->schema->keySize+rel2->schema->keySize));

  	for (i = 0; i < rel1_schema->numAttr; ++i, ++attrs) {
  		attrNames[attrs] = malloc(strlen(rel1_schema->attrNames[i])+1);
  		strcpy(attrNames[attrs],rel1_schema->attrNames[i]);
  	}

  	for (i = 0; i < rel2_schema->numAttr; ++i, ++attrs) {
  		attrNames[attrs] = malloc(strlen(rel2_schema->attrNames[i])+1);
  		strcpy(attrNames[attrs],rel2_schema->attrNames[i]);
  	}

  	memcpy(dataTypes, rel1_schema->dataTypes, sizeof(DataType) * rel1_schema->numAttr);
  	memcpy(sizes, rel1_schema->typeLength, sizeof(int) * rel1_schema->numAttr);
	memcpy(keys, rel1_schema->keyAttrs, sizeof(int) * rel1_schema->keySize);

	memcpy(&dataTypes[rel1_schema->numAttr], rel2_schema->dataTypes, sizeof(DataType) * rel2_schema->numAttr);
  	memcpy(&sizes[rel1_schema->numAttr], rel2_schema->typeLength, sizeof(int) * rel2_schema->numAttr);
	memcpy(&keys[rel1_schema->keySize], rel2_schema->keyAttrs, sizeof(int) * rel2_schema->keySize);

	schema = createSchema (numAttr, attrNames, dataTypes, sizes, rel1_schema->keySize + rel2_schema->keySize, keys);

	return schema;
}

/*
	Concat 2 records and insert in relation rel
*/
static RC
concat_records_and_insert(RM_TableData *rel, RM_TableData *rel1, RM_TableData *rel2)
{
	RC status = RC_OK;

	Record *record1 = NULL;
	Record *record2 = NULL;
	Record *record = NULL;

	RID rid1 = {.page = -1,.slot = -1};
	RID rid2 = {.page = -1,.slot = -1};
	//Concat records & insert
	do {
		//read records from r1
		createRecord (&record1, rel1->schema);
		status = getNextRecord(rel1, &rid1, record1);
		if (status != RC_OK)
		{
			freeRecord(record1);
			return status;
		}
		rid2.page=-1;
		rid2.slot=-1;
		do {
			createRecord (&record2, rel2->schema);
			status = getNextRecord(rel2, &rid2, record2);
			if (status != RC_OK)
			{
				freeRecord(record2);
				break;
			}
			createRecord(&record, rel->schema);
			memcpy(record->data, record1->data, getRecordSize(rel1->schema));
			memcpy(&record->data[getRecordSize(rel1->schema)], record2->data, getRecordSize(rel2->schema));
			insertRecord (rel, record);
			freeRecord(record);
			freeRecord(record2);
		}while(TRUE);

			
		freeRecord(record1);
	}while(TRUE);
}