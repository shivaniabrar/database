#include "hash.h"
#include "dt.h"
#include <stdlib.h>

void InitHashTable(HashTable htable){
	int i;
	for (i = 0; i < HASH_TABLE_SIZE; ++i)
	{
		InitListNode(&htable[i]);
	}
}

void Insert(HashTable htable,Key key,Value value){
	int bucket;
	bucket = hash(key);
	AddToFront(&htable[bucket],value);
}

void Remove(HashTable htable,Value value){
	RemoveFromList(value);
}

Value Search(HashTable htable,Key key,SearchFunc search){
	int bucket,status;
	bucket = hash(key);
	Value node;
	ListNode * head = &htable[bucket];
	for (node = head->next; node != head; node = node->next){ 
    	status = search(key,node);    
    	if (status == true)
    	{
    	    return node;
    	}
   	}
	return NULL;
}

int hash(Key key){
	return (key % HASH_TABLE_SIZE);
}
