/**
  * Handles encoding and decoding 
  */
#include "transcoder.h"

/**
  * Returns encoded schema
  */
char *
EncodeSchema(char *buffer_ptr, Schema *schema)
{
	int attr = 0;

	// numAttrs
	buffer_ptr = ENCODE(buffer_ptr, schema->numAttr);
	// attrNames
	for (attr = 0; attr < schema->numAttr; ++attr)
	{
		buffer_ptr = ENCODE_STRING(buffer_ptr, schema->attrNames[attr]);
		buffer_ptr = ENCODE(buffer_ptr, schema->dataTypes[attr]);
		buffer_ptr = ENCODE(buffer_ptr, schema->typeLength[attr]);
	}

	// numKeyAttrs
	buffer_ptr = ENCODE(buffer_ptr, schema->keySize);
	// keyAttrs
	for (attr = 0; attr < schema->keySize; ++attr)
	{
		buffer_ptr = ENCODE(buffer_ptr, schema->keyAttrs[attr]);
	}

	return buffer_ptr;
}

/**
  * Decodes schema
  */
char *
DecodeSchema(char *buffer_ptr, Schema *schema)
{
	int attr = 0;

	// numAttrs
	buffer_ptr = DECODE(buffer_ptr, schema->numAttr);
	schema->attrNames = (char **)malloc(schema->numAttr * sizeof(char *));
	schema->dataTypes = (uint32_t *)malloc(schema->numAttr * sizeof(uint32_t));
	schema->typeLength = (int *)malloc(schema->numAttr * sizeof(int));
	// attrNames
	for (attr = 0; attr < schema->numAttr; ++attr)
	{
		buffer_ptr = DECODE_STRING(buffer_ptr, schema->attrNames[attr]);
		buffer_ptr = DECODE(buffer_ptr, schema->dataTypes[attr]);
		buffer_ptr = DECODE(buffer_ptr, schema->typeLength[attr]);
	}

	// numKeyAttrs
	buffer_ptr = DECODE(buffer_ptr, schema->keySize);
	schema->keyAttrs = (int *)malloc(schema->keySize * sizeof(int));
	// keyAttrs
	for (attr = 0; attr < schema->keySize; ++attr)
	{
		buffer_ptr = DECODE(buffer_ptr, schema->keyAttrs[attr]);
	}

	return buffer_ptr;
}

/**
  * Returns encoded record
  */
char *
EncodeRecord (TableHeader *tableHeader, char *page, Record *record)
{
	char *slot = page + record_offset(tableHeader,record->id.slot);
	memcpy(slot,record->data,tableHeader->recordSize);
	return page + record_offset(tableHeader,record->id.slot + 1);
}


/**
  * Decodes record
  */
char *
DecodeRecord (TableHeader *tableHeader, char *page, Record *record)
{
	char *slot = page + record_offset(tableHeader,record->id.slot);
	memcpy(record->data,slot,tableHeader->recordSize);
	return page + record_offset(tableHeader,record->id.slot + 1);
}