/**
  * Handles creations, deletion and modification of the records
  */
  
#include <stdlib.h>
#include <strings.h>

#include "record_mgr.h"
#include "datatypes.h"

static int getAttrOffset(Schema *schema, int attrNum);
static void *getAttrPointer(Record *record,Schema *schema,int attrNum);

// dealing with records and attribute values

/**
  * Allocates space for record and initializes record
  */
RC 
createRecord (Record **record, Schema *schema)
{
	Record *rec;
	if (schema == NULL)
	{
		return RC_SCHEMA_NOT_INIT;
	}
	//Allocates space for record
	rec = (Record *)malloc(sizeof(Record));
	
	rec->id.page=rec->id.slot=0;
	rec->data = (char *)malloc(MAX(getRecordSize(schema),8));
	bzero(rec->data,getRecordSize(schema));

	*record = rec;
	return RC_OK;
}

/**
  * Free space for the record 
  */
RC 
freeRecord (Record *record)
{
	//DeIntialize record
	if (record == NULL)
	{
		return RC_RECORD_NOT_INIT;
	}
	free(record->data);
	bzero(record,sizeof(Record));
	free(record);
	return RC_OK;	
}

/**
  * Returns value of attribute in a record
  */
RC 
getAttr (Record *record, Schema *schema, int attrNum, Value **value)
{
	void *attrptr = NULL;
	Value *val = NULL;

	//Get pointer to attribute attrNum
	attrptr = getAttrPointer(record,schema,attrNum);
	
	val = (Value *)malloc(sizeof(Value));
	//Get datatype of attribute attrNum
	val->dt = schema->dataTypes[attrNum];
	//Get value of attribute attrNum in val
	dataTypes_table[val->dt]->getAttrValue(schema,attrptr,val,attrNum);
	*value = val; 
	return RC_OK;	
}

/**
  * Sets value of attribute in a record
  */
RC 
setAttr (Record *record, Schema *schema, int attrNum, Value *value)
{
	void *attrptr = NULL;
	//Validates value datatype with table datatype
	if (schema->dataTypes[attrNum] != value->dt)
	{
		return RC_MISMATCH_DATATYPE;
	}
	//Get pointer to attribute attrNum
	attrptr = getAttrPointer(record,schema,attrNum);
	//Set attribute for the attribute attrNum
	dataTypes_table[value->dt]->setAttrValue(attrptr,value);
	return RC_OK;
}

/**
  * Returns size of the record
  */
int getRecordSize (Schema *schema)
{
	int size;
	size = getAttrOffset(schema,schema->numAttr);
	return size;
}

//Utility functions

/**
  * Returns pointer to the attribute attrNum in Record record
  */
static void *
getAttrPointer(Record *record,Schema *schema,int attrNum)
{
	void *attrptr = NULL;
	int offset = 0;
	offset = getAttrOffset(schema,attrNum);
	attrptr = (void *)record->data + offset;
	return attrptr;
}

/**
  * Returns offset to the attribute attrNum in Record record
  */
static int
getAttrOffset(Schema *schema, int attrNum)
{
	int attr = 0;
	int offset = 0;

	for (attr = 0; attr < attrNum ; ++attr) 
	{	//Add size of the datatypes
		offset += dataTypes_table[schema->dataTypes[attr]]->size(schema,attr);
	}

	return offset;
}