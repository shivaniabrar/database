/**
  * Handles alloation and free space for record on page file 
  */

#include <stdlib.h>

#include "recordAllocator.h"

static int AllocateRecordPage(TableMgmtData *mgmtData);
static int InitRecordPage(TableHeader *tableHeader, char *page);
uint64_t slot_offset (TableHeader *tableHeader,uint64_t slot_number);

/**
  * Allocates space for new record on page file
  * Returns rid of new record
  */
RID
AllocateRecord(TableMgmtData *mgmtData)
{
	TableHeader *tableHeader = &mgmtData->tableHeader;
	BM_PageHandle *ph = NULL;
	RID rid;
	PageHeader *pageHeader = NULL;
	char *page = NULL;
	uint64_t slot = 0;

	//Initialize rid of the record
	rid.page = -1;
	rid.slot = -1;

	//Checks if there is free space in exisiting pages
	if (tableHeader->free_page_start == -1) {
		AllocateRecordPage(mgmtData);
	}

	//Validates if tableHeader is initialized 
	if (tableHeader->free_page_start < 2)
	{
		return rid;
	}

	ph = MAKE_PAGE_HANDLE();

	//Pin page having free space for record
	pinPage(mgmtData->bp,ph,tableHeader->free_page_start);
	page = ph->data;
	pageHeader = (PageHeader *)page;
	
	//Check if free space in page
	if (pageHeader->free_slot_start == -1)
	{
		return rid;
	}

	//Gets number of free slot
	slot = pageHeader->free_slot_start;
	++tableHeader->num_tuples;
	
	//Updates page header
	pageHeader->free_slot_start = *(int *)(page + record_offset(tableHeader, slot));
	if (pageHeader->free_slot_start == -1) {
		tableHeader->free_page_start = pageHeader->next_free_page;
	}
	
	//update record header
	((RecordHeader *)(page + slot_offset(tableHeader,slot)))->allocated = true;
	unpinPage(mgmtData->bp, ph);
	rid.page = ph->pageNum;
	rid.slot = slot;
	free(ph);
	return rid;
}

/**
  * Place tombstone for the record
  */
int
FreeRecord(TableMgmtData *mgmtData, RID rid)
{
	TableHeader *tableHeader = &mgmtData->tableHeader;
	BM_PageHandle *ph = NULL;;
	char *page = NULL;
	PageHeader *pageHeader = NULL;

	if (rid.page < 0 && 
		rid.page >= tableHeader->num_pages &&
		rid.slot < 0 &&
		rid.slot >= tableHeader->slots_in_page) {
		return 1;
	}

	ph = MAKE_PAGE_HANDLE();
	pinPage(mgmtData->bp,ph,rid.page);
	page = ph->data;

	//update table header and page header
	pageHeader = (PageHeader *)page;
	if (pageHeader->next_free_page == -1) {
		pageHeader->next_free_page = tableHeader->free_page_start;
		tableHeader->free_page_start = ph->pageNum;
	}

	//Place tombstone
	*(int *)(page + record_offset(tableHeader, rid.slot)) = pageHeader->free_slot_start;
	pageHeader->free_slot_start = rid.slot;
	((RecordHeader *)(page + slot_offset(tableHeader,rid.slot)))->allocated = false;
	markDirty(mgmtData->bp,ph);
	--tableHeader->num_tuples;
	unpinPage(mgmtData->bp,ph);
	free(ph);
	return 0;
}

/**
  * Allocates new page for the record 
  */
static int
AllocateRecordPage(TableMgmtData *mgmtData)
{
	TableHeader *tableHeader = &mgmtData->tableHeader;
	BM_PageHandle *ph = NULL;
	char *page;

	//Checks if there is page having free space for record
	if (tableHeader->free_page_start != -1)
	{
		return 1;
	}

	ph = MAKE_PAGE_HANDLE();

	//Updates number of pages
	tableHeader->free_page_start = tableHeader->num_pages;
	tableHeader->num_pages += 1;

	//Init new page
	pinPage(mgmtData->bp,ph,tableHeader->free_page_start);
	page = ph->data;
	InitRecordPage(tableHeader, page);
	markDirty(mgmtData->bp,ph);
	unpinPage(mgmtData->bp, ph);

	free(ph);
	return 0;
}

/**
  * Initializes new page
  */
static int
InitRecordPage(TableHeader *tableHeader, char *page)
{

	uint64_t i;
	uint64_t slots = tableHeader->slots_in_page;
	InitPageHeader((PageHeader *)page);

	//Inits free list
	for (i = 0; i < slots - 1; ++i) {
		InitRecordHeader((RecordHeader *)(page + slot_offset(tableHeader,i)));
	    *(int *)(page + record_offset(tableHeader, i)) = i + 1;
	}

	InitRecordHeader((RecordHeader *)(page + slot_offset(tableHeader,slots - 1)));
	*(int *)(page + record_offset(tableHeader, slots - 1)) = -1;

	return 0;
}

/**
  * Checks if record exists
  */
bool 
IsRecordExist(TableHeader *tableHeader, char *page, int slot)
{
	return ((RecordHeader *)(page + slot_offset(tableHeader,slot)))->allocated;
}

/**
  * Returns offset of the record in a page
  */
uint64_t 
record_offset (TableHeader *tableHeader,uint64_t slot_number)
{
	uint64_t offset = 0;
	offset = sizeof(PageHeader) + (tableHeader->recordHeaderSize + tableHeader->recordSize) * slot_number + tableHeader->recordHeaderSize;
	return offset;
} 

/**
  * Returns offset of the slot in a page
  */
uint64_t 
slot_offset (TableHeader *tableHeader,uint64_t slot_number)
{
	uint64_t offset = 0;
	offset = sizeof(PageHeader) + (tableHeader->recordHeaderSize + tableHeader->recordSize) * slot_number;
	return offset;
} 