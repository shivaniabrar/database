/**
  * Implement functions to handle different datatypes
  */
#include <strings.h>
#include <string.h>
#include <stdlib.h>

#include "datatypes.h"

/**
  * Sets value of attribute of datatype int
  */
static int
setAttrValueInt(void *attrptr,Value *value)
{
	*((int *)attrptr)=value->v.intV;
	return 0;
}

/**
  * Sets value of attribute of datatype float
  */
static int
setAttrValueFloat(void *attrptr,Value *value)
{
	*((float *)attrptr)=value->v.floatV;
	return 0;
}

/**
  * Sets value of attribute of datatype bool
  */
static int
setAttrValueBool(void *attrptr,Value *value)
{
	*((bool *)attrptr)=value->v.boolV;
	return 0;
}

/**
  * Sets value of attribute of datatype string
  */
static int
setAttrValueString(void *attrptr,Value *value)
{
	strncpy(attrptr,value->v.stringV,strlen(value->v.stringV));
	return 0;
}

/**
  * Gets value of attribute of datatype int
  */
static int
getAttrValueInt(Schema *schema,void *attrptr,Value *value, int attr)
{
	value->v.intV = *((int *)attrptr);	
	return 0;
}

/**
  * Gets value of attribute of datatype float
  */
static int
getAttrValueFloat(Schema *schema,void *attrptr,Value *value, int attr)
{
	value->v.floatV = *((float *)attrptr);	
	return 0;
}

/**
  * Gets value of attribute of datatype bool
  */
static int
getAttrValueBool(Schema *schema,void *attrptr,Value *value, int attr)
{
	value->v.boolV = *((bool *)attrptr);	
	return 0;
}

/**
  * Gets value of attribute of datatype string
  */
static int
getAttrValueString(Schema *schema,void *attrptr,Value *value, int attr)
{
	char *stringV = malloc(schema->typeLength[attr]+1);
	bzero(stringV,schema->typeLength[attr]+1);
	strncpy(stringV,attrptr,schema->typeLength[attr]);
	value->v.stringV = stringV;	
	return 0;
}

/**
  * Returns size of int
  */
static int
sizeInt(Schema *schema,int attr)
{
	return sizeof(int);
}

/**
  * Returns size of float
  */
static int
sizeFloat(Schema *schema,int attr)
{
	return sizeof(float);
}

/**
  * Returns size of bool
  */
static int
sizeBool(Schema *schema,int attr)
{
	return sizeof(bool);
}

/**
  * Returns size of string uses typeLength
  */
static int
sizeString(Schema *schema,int attr)
{
	return schema->typeLength[attr];
}

/**
  * Class of functions for int datatype
  */
static dataTypes_t int_t = {
	.setAttrValue = setAttrValueInt,
	.getAttrValue = getAttrValueInt,
	.size = sizeInt,
};

/**
  * Class of functions for float datatype
  */
static dataTypes_t float_t = {
	.setAttrValue = setAttrValueFloat,
	.getAttrValue = getAttrValueFloat,
	.size = sizeFloat,
};

/**
  * Class of functions for bool datatype
  */
static dataTypes_t bool_t = {
	.setAttrValue = setAttrValueBool,
	.getAttrValue = getAttrValueBool,
	.size = sizeBool,
};

/**
  * Class of functions for string datatype
  */
static dataTypes_t string_t = {
	.setAttrValue = setAttrValueString,
	.getAttrValue = getAttrValueString,
	.size = sizeString,
};

/**
  * Maps datatype to class of functions handling that datatype
  */
dataTypes_t *dataTypes_table[NO_OF_DATA_TYPES] = {
	&int_t,
	&string_t,
	&float_t,
	&bool_t
};