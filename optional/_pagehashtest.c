#include <stdio.h>
#include "PageHash.c"
#include "hash.h"
#define MAX 10000
int main(int argc, char const *argv[])
{

	PageHead ph[MAX],*p;
	int i;
	HashTable htable;
	InitHashTable(htable);
	for (i = 0; i < MAX; ++i)
	{
		ph[i].pageNum = i;
		InsertPage(htable,&ph[i]);
	}

	for (i = 0; i < MAX; ++i)
	{
		p = SearchPage(htable,i);	
		printf("%d\n",p->pageNum);
	}	

	p = SearchPage(htable,100000);


	if (p==NULL)
	{
		printf("false\n");
	}

	for (i = 0; i < MAX; ++i)
	{
		RemovePage(htable,&ph[i]);	
	}	

	for (i = 0; i < MAX; ++i)
	{
		p = SearchPage(htable,i);
		if (p==NULL)
		{
			printf("false\n");
		}
	}	


	return 0;
}