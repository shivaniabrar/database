#include <stdio.h>

#include "transcoder.h"
#include "dberror.h"
#include "record_mgr.h"

Schema *testSchema(void);

int 
main(int argc, char const *argv[])
{
	Schema *encode_schema, *decode_schema;
	char *page,*page_ptr;
	char *encoded_schema,*decoded_schema;

	encode_schema = testSchema();
	page = malloc(PAGE_SIZE);
	decode_schema = malloc(sizeof(Schema));
	
	page_ptr = page;
	EncodeSchema(page_ptr,encode_schema);
	page_ptr = page;
	DecodeSchema(page_ptr,decode_schema);
	
	encoded_schema = serializeSchema(encode_schema);
	decoded_schema = serializeSchema(decode_schema);

	printf("Encoded Schema: %s",encoded_schema);
	printf("Decoded Schema: %s",decoded_schema);
	printf("Result: %s\n",strcmp(encoded_schema,decoded_schema)==0?"MATCH":"NOT MATCH");

	free(encoded_schema);
	free(decoded_schema);
	freeSchema(decode_schema);
	freeSchema(encode_schema);
	free(page);
	return 0;
}

Schema *
testSchema (void)
{
  Schema *result;
  char *names[] = { "a", "b", "c" };
  DataType dt[] = { DT_INT, DT_STRING, DT_INT };
  int sizes[] = { 0, 4, 0 };
  int keys[] = {0};
  int i;
  char **cpNames = (char **) malloc(sizeof(char*) * 3);
  DataType *cpDt = (DataType *) malloc(sizeof(DataType) * 3);
  int *cpSizes = (int *) malloc(sizeof(int) * 3);
  int *cpKeys = (int *) malloc(sizeof(int));

  for(i = 0; i < 3; i++)
    {
      cpNames[i] = (char *) malloc(2);
      strcpy(cpNames[i], names[i]);
    }
  memcpy(cpDt, dt, sizeof(DataType) * 3);
  memcpy(cpSizes, sizes, sizeof(int) * 3);
  memcpy(cpKeys, keys, sizeof(int));

  result = createSchema(3, cpNames, cpDt, cpSizes, 1, cpKeys);

  return result;
}
