#include <stdio.h>
#include "list.h"
#include <stddef.h>

typedef struct test{
	int value;
	ListNode link;	
}Test;

int main(int argc, char const *argv[])
{
	ListNode head;
	InitListNode(&head);
	Test t1;
	InitListNode(&(t1.link));
	Test *t2;
	unsigned int off;
	t1.value = 100;
	AddToBack(&head,&t1.link);
	off = offsetof(Test,link);
	//adr = &(head->next);
	//toff = (char *)(&(t1->link))-(char *)t1;
	//t2 = (Test *)((char *)((head.next)))-((char *)off);
	t2 = GetEnclosingTypePtr(Test, link, head.next);
	printf("%d\n",t2->value);


	return 0;
}