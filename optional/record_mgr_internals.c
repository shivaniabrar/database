/**
  * Initializes internal datastructures for thr record manager
  */
#include <stdlib.h>
#include <string.h>
#include <assert.h>
  
#include "record_mgr.h"
#include "record_mgr_internals.h"

//Returns record header size
static uint64_t RecordHeaderSize(Schema *schema);

//Init page header
char *
InitPageHeader(PageHeader *ph)
{
	ph->next_free_page = -1;
	ph->free_slot_start = 0;
	bzero(ph->padding,sizeof(uint64_t) * PAGE_HEADER_PADDING);
	return (char *)ph + sizeof(PageHeader);
}

//Init record header
int
InitRecordHeader(RecordHeader *rh)
{
	rh->allocated = false;
	return 0;
}

//Init Scan handle Mgmt data used by scan functions
int
InitScanHandleMgmtData(ScanHandleMgmtData *mgmtData, TableHeader *tableHeader, Expr *expr)
{
	mgmtData->expr = expr;
	mgmtData->rid.page = -1;
	mgmtData->rid.slot = -1;
	return 0;
}

/**
  * Initializes table header which stores table metadata information
  */
int
CreateTableHeader(TableHeader *tableHeader, Schema *schema)
{
	memset(tableHeader, 0, sizeof(*tableHeader));
	tableHeader->free_page_start = -1;
	tableHeader->recordSize = MAX(getRecordSize(schema),8);
	tableHeader->recordHeaderSize = RecordHeaderSize(schema);
	tableHeader->slots_in_page = (PAGE_SIZE - sizeof(PageHeader))/(tableHeader->recordSize + tableHeader->recordHeaderSize);
	tableHeader->num_tuples = 0;
	tableHeader->num_pages = RECORD_START_PAGE;
	return 0;
}

TableMgmtData *
CreateTableMgmtData()
{
	TableMgmtData *mgmtData;
	mgmtData = malloc(sizeof(TableMgmtData));
	mgmtData->bp = MAKE_POOL();
	return mgmtData;
}

static uint64_t
RecordHeaderSize(Schema *schema) 
{
	uint64_t size = 0;
	size += RESERVED_BITS;
	size += ((schema->numAttr-1) / (sizeof(char) * 8)) + 1;
	return size;
}

/*
	Gets Next Record from page file.
*/
RC
getNextRecord(RM_TableData *rel, RID *rid, Record *record)
{

	RC rc_status = RC_OK;
	do {
		//Gets rid of next record 
		rc_status = getNextRid(rel, rid);
		if (rc_status != RC_OK)
		{
			return rc_status;
		}
		//Gets next record
		rc_status = getRecord(rel, *rid, record);
	}while(rc_status != RC_OK);

	return rc_status;
}

/*
	Returns rid of next record.
*/
RC 
getNextRid(RM_TableData *rel, RID *rid)
{
	TableMgmtData *table_mgmtData = rel->mgmtData;

	if (rid->page == -1) {
		rid->page = RECORD_START_PAGE;
	}

	assert(rid->page >= RECORD_START_PAGE);
	++rid->slot;
	assert(rid->slot >= 0);
	if (rid->slot == table_mgmtData->tableHeader.slots_in_page)
	{
		rid->slot = 0;
		++rid->page;
		if (rid->page == table_mgmtData->tableHeader.num_pages)
		{
			rid->page = -1;
			rid->slot = -1;
			return RC_RM_NO_MORE_TUPLES;
		}
	}
	return RC_OK;
}

/*
	Matches record with the given condition.
*/
bool
matchRecord(RM_TableData *rel, Record *record, Expr *expr)
{
	Value *result = NULL;
	bool status = FALSE;
	//Check if record satisfies the condition
	evalExpr(record, rel->schema, expr, &result);
	status = result->v.boolV;
	freeVal(result); 
	return status;
}