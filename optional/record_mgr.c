#include <stdlib.h>
#include <strings.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#include "record_mgr.h"
#include "datatypes.h"
#include "buffer_mgr.h"
#include "transcoder.h"
#include "storage_mgr.h"
#include "record_mgr_internals.h"
#include "recordAllocator.h"

// manager functions
RC 
initRecordManager (void *mgmtData)
{
	return RC_OK;
}

RC 
shutdownRecordManager ()
{
	return RC_OK;
}

// table functions

/*
	Creates Page file stores metadata about table in page file 
*/
RC createTable (char *name, Schema *schema)
{
	RC status;
	BM_BufferPool *bp = NULL;
	BM_PageHandle *ph = NULL;
	TableHeader tableHeader;

	status = createPageFile(name);
	if (status != RC_OK){
		return status;
	}

	//Creates table headers
	CreateTableHeader(&tableHeader,schema);		

	ph = MAKE_PAGE_HANDLE();
	bp = MAKE_POOL();
	initBufferPool(bp, name, 2, RS_FIFO, NULL);

	//stores schema information on page file
	pinPage(bp,ph,SCHEMA_PAGE);
	EncodeSchema(ph->data, schema);
	markDirty(bp,ph);
	unpinPage(bp,ph);

	//stores table metadata information on page file
	pinPage(bp,ph,TABLE_HEADER_PAGE);
	ENCODE(ph->data, tableHeader);
	markDirty(bp,ph);
	unpinPage(bp,ph);	

	shutdownBufferPool(bp);
	free(bp);
	free(ph);
	return RC_OK;
}

/*
	Opens page file of the table.
	Reads schema and table metadata information from pagefile using buffer pool.
*/
RC openTable (RM_TableData *rel, char *name)
{
	TableMgmtData *mgmtData = NULL;
	BM_PageHandle *ph = NULL;

	mgmtData = CreateTableMgmtData();
	ph = MAKE_PAGE_HANDLE();

	rel->name = name;
	rel->mgmtData = mgmtData;
	rel->schema = malloc(sizeof(Schema));

	initBufferPool(mgmtData->bp,name,1024,RS_FIFO,NULL);

	//Reads schema information from page file
	pinPage(mgmtData->bp,ph,SCHEMA_PAGE);
	DecodeSchema(ph->data, rel->schema);
	unpinPage(mgmtData->bp,ph);

	//Reads table metadata information from page file
	pinPage(mgmtData->bp,ph,TABLE_HEADER_PAGE);
	DECODE(ph->data, mgmtData->tableHeader);
	markDirty(mgmtData->bp,ph);
	unpinPage(mgmtData->bp,ph);	

	free(ph);
	return RC_OK;
}

/*
	Update metadata of table 
*/
RC closeTable (RM_TableData *rel)
{
	BM_PageHandle *ph = NULL;
	TableMgmtData *mgmtData = NULL;

	mgmtData = rel->mgmtData;

	ph = MAKE_PAGE_HANDLE();

	//Updates metadat information on the page
	pinPage(mgmtData->bp,ph,TABLE_HEADER_PAGE);
	ENCODE(ph->data, mgmtData->tableHeader);
	markDirty(mgmtData->bp,ph);
	unpinPage(mgmtData->bp,ph);

	shutdownBufferPool(mgmtData->bp);
	free(mgmtData->bp);
	free(rel->mgmtData);
	freeSchema(rel->schema);
	free(ph);
	return RC_OK;
}

/*
	Destroy's page file
*/
RC deleteTable (char *name)
{
	destroyPageFile(name);
	return RC_OK;
}


/*
	Get number of tuples which is stored in metadata information of the table.
*/
int getNumTuples (RM_TableData *rel)
{

	int num_tuples = 0;
	TableMgmtData *mgmtData = rel->mgmtData;
	num_tuples = mgmtData->tableHeader.num_tuples;
	return num_tuples;

}

// handling records in a table

/*
	Allocates a record on page 
	Gets rid of the record
	Stores the record in page  
*/
RC insertRecord (RM_TableData *rel, Record *record)
{
	BM_PageHandle *ph = NULL;
	TableMgmtData *mgmtData = NULL;
	char *page;
	mgmtData = rel->mgmtData;

	//Allocate record on page file
	record->id = AllocateRecord((TableMgmtData *)rel->mgmtData);
	
	ph = MAKE_PAGE_HANDLE();

	//Store record on page
	pinPage(mgmtData->bp,ph,record->id.page);
	page = ph->data;
	EncodeRecord(&mgmtData->tableHeader,page,record);
	markDirty(mgmtData->bp,ph);
	unpinPage(mgmtData->bp,ph);
	free(ph);

	return RC_OK;
}

/*
	Free record with rid id
*/
RC deleteRecord (RM_TableData *rel, RID id)
{
	TableMgmtData *mgmtData = NULL;
	mgmtData = rel->mgmtData;

	FreeRecord(mgmtData,id);

	return RC_OK;
}

/*
	Gets page with pageNum in rid
	Update record in page
*/
RC updateRecord (RM_TableData *rel, Record *record)
{
	BM_PageHandle *ph = NULL;
	TableMgmtData *mgmtData = NULL;
	char *page;
	mgmtData = rel->mgmtData;

	ph = MAKE_PAGE_HANDLE();

	//Store updated record in page 
	pinPage(mgmtData->bp,ph,record->id.page);
	page = ph->data;
	EncodeRecord(&mgmtData->tableHeader,page,record);
	unpinPage(mgmtData->bp,ph);

	free(ph);
	return RC_OK;
}

/*
	Checks whether record exist.
	Gets record from page file using buffer pool.
*/	
RC
getRecord(RM_TableData *rel, RID id, Record *record)
{
	BM_PageHandle *ph = NULL;
	TableMgmtData *mgmtData = rel->mgmtData;
	char *page = NULL;

	ph = MAKE_PAGE_HANDLE();
	record->id = id;

	//Check if record exists
	pinPage(mgmtData->bp, ph, id.page);
	page = ph->data;
	if (IsRecordExist(&mgmtData->tableHeader, page, id.slot) == false) {
		unpinPage(mgmtData->bp, ph);
		free(ph);
		return RC_RECORD_NOT_EXIST;
	}

	DecodeRecord(&mgmtData->tableHeader, page, record);
	unpinPage(mgmtData->bp, ph);

	free(ph);
	return RC_OK;
}

// scans

/*
	Initializes scan handle.
*/
RC startScan(RM_TableData *rel, RM_ScanHandle *scan, Expr *cond)
{
	ScanHandleMgmtData *mgmtData = NULL;
	TableMgmtData *table_mgmtData = rel->mgmtData;

	mgmtData = malloc(sizeof(ScanHandleMgmtData));

	scan->rel = rel;
	scan->mgmtData = mgmtData;

	InitScanHandleMgmtData(mgmtData,&table_mgmtData->tableHeader,cond);

	return RC_OK;
}

/*
	Returns next tuple which satisfies the given expression.
	Returns RC_RM_NO_MORE_TUPLES if no more tuples satisfies the information.
*/
RC 
next(RM_ScanHandle *scan, Record *record)
{
	ScanHandleMgmtData *mgmtData = scan->mgmtData;
	RC rc_status = RC_OK;
	bool matchFound = FALSE;

	do {
		//Get next record
		rc_status = getNextRecord(scan->rel, &mgmtData->rid, record);
		if (rc_status != RC_OK)
		{
			return rc_status;
		}
		//Match the record to check if the record satifies the condition
		matchFound = matchRecord(scan->rel, record, mgmtData->expr);
	}while(!matchFound);

	return rc_status;
}

RC closeScan (RM_ScanHandle *scan)
{
	free(scan->mgmtData);
	return RC_OK;
}

/*
	Updates the record which satisfies the given condition.
*/
RC 
updateScan(RM_TableData *rel, Expr *cond, RC (*updateTuple)(RM_TableData *rel, Record *))
{
	RM_ScanHandle scan;
	RC rc_status = RC_OK;
	Record record;
	TableMgmtData *mgmtData = rel->mgmtData;

	record.data = malloc(mgmtData->tableHeader.recordSize);
	
	//Start scan for records
	startScan(rel, &scan, cond);
	//Updates matched tuples
	while( (rc_status = next(&scan,&record)) == RC_OK) {
		updateTuple(rel,&record);
		updateRecord(rel,&record);
	}
	closeScan(&scan);

	free(record.data);
	return RC_OK;
}


// dealing with schemas

/*
	Allocates and initiliaze schema
*/
Schema *
createSchema (int numAttr, char **attrNames, DataType *dataTypes, int *typeLength, int keySize, int *keys)
{
	Schema *schema;

	//Allocate Space for schema
	schema = (Schema *)malloc(sizeof(Schema));
	bzero(schema,sizeof(Schema));
	
	//Initialize Schema
	schema->numAttr = numAttr;
	schema->attrNames = attrNames;
	schema->dataTypes = dataTypes;
	schema->typeLength = typeLength;
	schema->keyAttrs = keys;
	schema->keySize = keySize;

	return schema;
}

/*
	Free Schema
*/
RC 
freeSchema (Schema *schema)
{
	//DeIntialize schema
	int attr;
	if (schema == NULL)
	{
		return RC_SCHEMA_NOT_INIT;
	}
	for (attr = 0; attr < schema->numAttr ; ++attr)
	{
		free(schema->attrNames[attr]);
	}
	free(schema->attrNames);
	free(schema->dataTypes);
	free(schema->typeLength);
	free(schema->keyAttrs);
	bzero(schema,sizeof(Schema));
	free(schema);
	return RC_OK;
}

/*
	Calculates size of schema
*/
uint64_t
SizeOfSchema(Schema *schema)
{
	int attr=0;
	uint64_t len = 0;

	// numAttr
	len += sizeof(int);
	// attrs
	for (attr = 0; attr < schema->numAttr ; ++attr)
	{
		// attr name size
		len += sizeof(uint64_t);
		len += strlen(schema->attrNames[attr]);
		// attr data type
		len += sizeof(uint32_t);
		// attr type len
		len += sizeof(int);
	}

	// numKeyAttrs
	len += sizeof(int);
	// keyAttrs
	len += sizeof(int)*schema->keySize;

	return len;
}