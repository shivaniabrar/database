#include <stdlib.h>
#include "dberror.h"
#include "expr.h"
#include "record_mgr.h"
#include "tables.h"
#include "test_helper.h"
#include "optionals.h"

#define ASSERT_EQUALS_RECORDS(_l,_r, schema, message)			\
  do {									\
    Record *_lR = _l;                                                   \
    Record *_rR = _r;                                                   \
    ASSERT_TRUE(memcmp(_lR->data,_rR->data,getRecordSize(schema)) == 0, message); \
    int i;								\
    for(i = 0; i < schema->numAttr; i++)				\
      {									\
        Value *lVal, *rVal;                                             \
		char *lSer, *rSer; \
        getAttr(_lR, schema, i, &lVal);                                  \
        getAttr(_rR, schema, i, &rVal);                                  \
		lSer = serializeValue(lVal); \
		rSer = serializeValue(rVal); \
        ASSERT_EQUALS_STRING(lSer, rSer, "attr same");	\
		freeVal(lVal); \
		freeVal(rVal); \
		free(lSer); \
		free(rSer); \
      }									\
  } while(0)

#define ASSERT_EQUALS_RECORD_IN(_l,_r, rSize, schema, message)		\
  do {									\
    int i;								\
    boolean found = false;						\
    for(i = 0; i < rSize; i++)						\
      if (memcmp(_l->data,_r[i]->data,getRecordSize(schema)) == 0)	\
	found = true;							\
    ASSERT_TRUE(0, message);						\
  } while(0)

#define OP_TRUE(left, right, op, message)		\
  do {							\
    Value *result = (Value *) malloc(sizeof(Value));	\
    op(left, right, result);				\
    bool b = result->v.boolV;				\
    free(result);					\
    ASSERT_TRUE(b,message);				\
   } while (0)

// test methods
static void testRecords (void);
static void testCreateTableAndInsert (void);
static void testUpdateTable (void);
static void testScans (void);
static void testScansTwo (void);
static void testInsertManyRecords(void);
static void testMultipleScans(void);
static void testUpdateScan (void);
static void testUnion (void);
static void testNestedLoopJoin (void);

// struct for test records
typedef struct TestRecord {
  int a;
  char *b;
  int c;
} TestRecord;

// helper methods
Record *testRecord(Schema *schema, int a, char *b, int c);
Schema *testSchema (void);
Record *fromTestRecord (Schema *schema, TestRecord in);
RC updateTuple(RM_TableData *table, Record *record);
// test name
char *testName;

// main method
int 
main (void) 
{
    testName = "";

    testInsertManyRecords();
    testRecords();
    testCreateTableAndInsert();
    testUpdateTable();
    testScans();
    testScansTwo();
    testMultipleScans();
    testUpdateScan();
    testUnion();
    testNestedLoopJoin();
  
    return 0;
}

// ************************************************************ 
void
testRecords (void)
{
  TestRecord expected[] = { 
    {1, "aaaa", 3}, 
  };
  Schema *schema;
  Record *r;
  Value *value;
  Value *assert_value;
  testName = "test creating records and manipulating attributes";

  // check attributes of created record
  schema = testSchema();
  r = fromTestRecord(schema, expected[0]);

  getAttr(r, schema, 0, &value);
  assert_value = stringToValue("i1");
  OP_TRUE(assert_value, value, valueEquals, "first attr");
  freeVal(value);
  freeVal(assert_value);

  getAttr(r, schema, 1, &value);
  assert_value = stringToValue("saaaa");
  OP_TRUE(assert_value, value, valueEquals, "second attr");
  freeVal(value);
  freeVal(assert_value);

  getAttr(r, schema, 2, &value);
  assert_value = stringToValue("i3");
  OP_TRUE(assert_value, value, valueEquals, "third attr");
  freeVal(value);
  freeVal(assert_value);

  //modify attrs
  assert_value = stringToValue("i4");
  setAttr(r, schema, 2, assert_value);
  getAttr(r, schema, 2, &value);
  freeVal(assert_value);
  assert_value = stringToValue("i4");
  OP_TRUE(assert_value, value, valueEquals, "third attr after setting");
  freeVal(value);
  freeVal(assert_value);
  freeSchema(schema);
  freeRecord(r);
  TEST_DONE();
}

// ************************************************************ 
void
testCreateTableAndInsert (void)
{
  RM_TableData *table = (RM_TableData *) malloc(sizeof(RM_TableData));
  TestRecord inserts[] = { 
    {1, "aaaa", 3}, 
    {2, "bbbb", 2},
    {3, "cccc", 1},
    {4, "dddd", 3},
    {5, "eeee", 5},
    {6, "ffff", 1},
    {7, "gggg", 3},
    {8, "hhhh", 3},
    {9, "iiii", 2}
  };
  int numInserts = 9, i;
  Record *r,*assert_record;
  RID *rids;
  Schema *schema;
  testName = "test creating a new table and inserting tuples";
  schema = testSchema();
  rids = (RID *) malloc(sizeof(RID) * numInserts);
  
  TEST_CHECK(initRecordManager(NULL));
  TEST_CHECK(createTable("test_table_r",schema));
  TEST_CHECK(openTable(table, "test_table_r"));
  
  // insert rows into table
  for(i = 0; i < numInserts; i++)
    {
      r = fromTestRecord(schema, inserts[i]);
      TEST_CHECK(insertRecord(table,r)); 
      rids[i] = r->id;
      freeRecord(r);
    }

  TEST_CHECK(closeTable(table));
  TEST_CHECK(openTable(table, "test_table_r"));
  r = fromTestRecord(schema, inserts[0]);
  // randomly retrieve records from the table and compare to inserted ones
  for(i = 0; i < 1000; i++)
    {
      int pos = rand() % numInserts;
      RID rid = rids[pos];
      TEST_CHECK(getRecord(table, rid, r));
      assert_record = fromTestRecord(schema, inserts[pos]);
      ASSERT_EQUALS_RECORDS(assert_record, r, schema, "compare records");
      freeRecord(assert_record);
    }
  freeRecord(r);
  TEST_CHECK(closeTable(table));
  TEST_CHECK(deleteTable("test_table_r"));
  TEST_CHECK(shutdownRecordManager());

  freeSchema(schema);
  free(rids);
  free(table);
  TEST_DONE();
}

void
testMultipleScans(void)
{
  RM_TableData *table = (RM_TableData *) malloc(sizeof(RM_TableData));
  TestRecord inserts[] = { 
    {1, "aaaa", 3}, 
    {2, "bbbb", 2},
    {3, "cccc", 1},
    {4, "dddd", 3},
    {5, "eeee", 5},
    {6, "ffff", 1},
    {7, "gggg", 3},
    {8, "hhhh", 3},
    {9, "iiii", 2},
    {10, "jjjj", 5},
  };
  int numInserts = 10, i, scanOne=0, scanTwo=0;
  Record *r;
  RID *rids;
  Schema *schema;
  testName = "test running muliple scans ";
  schema = testSchema();
  rids = (RID *) malloc(sizeof(RID) * numInserts);
  RM_ScanHandle *sc1 = (RM_ScanHandle *) malloc(sizeof(RM_ScanHandle));
  RM_ScanHandle *sc2 = (RM_ScanHandle *) malloc(sizeof(RM_ScanHandle));
  Expr *se1, *left, *right;
  int rc,rc2;
  
  TEST_CHECK(initRecordManager(NULL));
  TEST_CHECK(createTable("test_table_r",schema));
  TEST_CHECK(openTable(table, "test_table_r"));
  
  // insert rows into table
  for(i = 0; i < numInserts; i++)
  {
      r = fromTestRecord(schema, inserts[i]);
      TEST_CHECK(insertRecord(table,r)); 
      rids[i] = r->id;
      freeRecord(r);
  }

  // Mix 2 scans with c=3 as condition
  MAKE_CONS(left, stringToValue("i3"));
  MAKE_ATTRREF(right, 2);
  MAKE_BINOP_EXPR(se1, left, right, OP_COMP_EQUAL);
  createRecord(&r, schema);
  TEST_CHECK(startScan(table, sc1, se1));
  TEST_CHECK(startScan(table, sc2, se1));
  if ((rc2 = next(sc2, r)) == RC_OK)
    scanTwo++;
  i = 0;
  while((rc = next(sc1, r)) == RC_OK)
  {
      scanOne++;
      i++;
      if (i % 3 == 0)
          if ((rc2 = next(sc2, r)) == RC_OK)
              scanTwo++;
  }
  while((rc2 = next(sc2, r)) == RC_OK)
    scanTwo++;

  ASSERT_TRUE(scanOne == scanTwo, "scans returned same number of tuples");
  if (rc != RC_RM_NO_MORE_TUPLES)
    TEST_CHECK(rc);
  TEST_CHECK(closeScan(sc1));
  TEST_CHECK(closeScan(sc2));
 
  TEST_CHECK(closeTable(table));
  TEST_CHECK(deleteTable("test_table_r"));
  TEST_CHECK(shutdownRecordManager());

  freeExpr(se1);
  freeRecord(r);
  free(sc1);
  free(sc2);
  freeSchema(schema);
  free(rids);
  free(table);
  TEST_DONE();
}

void 
testUpdateTable (void)
{
  RM_TableData *table = (RM_TableData *) malloc(sizeof(RM_TableData));
  TestRecord inserts[] = { 
    {1, "aaaa", 3}, 
    {2, "bbbb", 2},
    {3, "cccc", 1},
    {4, "dddd", 3},
    {5, "eeee", 5},
    {6, "ffff", 1},
    {7, "gggg", 3},
    {8, "hhhh", 3},
    {9, "iiii", 2},
    {10, "jjjj", 5},
  };
  TestRecord updates[] = {
    {1, "iiii", 6},
    {2, "iiii", 6},
    {3, "iiii", 6}
  };
  int deletes[] = {
    9,
    6,
    7,
    8,
    5
  };
  TestRecord finalR[] = {
    {1, "iiii", 6},
    {2, "iiii", 6},
    {3, "iiii", 6},
    {4, "dddd", 3},
    {5, "eeee", 5},
  };
  int numInserts = 10, numUpdates = 3, numDeletes = 5, numFinal = 5, i;
  Record *r,*assert_record;
  RID *rids;
  Schema *schema;
  testName = "test creating a new table and insert,update,delete tuples";
  schema = testSchema();
  rids = (RID *) malloc(sizeof(RID) * numInserts);
  
  TEST_CHECK(initRecordManager(NULL));
  TEST_CHECK(createTable("test_table_r",schema));
  TEST_CHECK(openTable(table, "test_table_r"));
  
  // insert rows into table
  for(i = 0; i < numInserts; i++)
    {
      r = fromTestRecord(schema, inserts[i]);
      TEST_CHECK(insertRecord(table,r)); 
      rids[i] = r->id;
      freeRecord(r);
    }

  // delete rows from table
  for(i = 0; i < numDeletes; i++)
    {
      TEST_CHECK(deleteRecord(table,rids[deletes[i]]));
    }

  // update rows into table
  for(i = 0; i < numUpdates; i++)
    {
      r = fromTestRecord(schema, updates[i]);
      r->id = rids[i];
      TEST_CHECK(updateRecord(table,r));
      freeRecord(r); 
    }

  TEST_CHECK(closeTable(table));
  TEST_CHECK(openTable(table, "test_table_r"));
  r = fromTestRecord(schema, updates[0]);
  // retrieve records from the table and compare to expected final stage
  for(i = 0; i < numFinal; i++)
    {
      RID rid = rids[i];
      TEST_CHECK(getRecord(table, rid, r));
      assert_record = fromTestRecord(schema, finalR[i]);
      ASSERT_EQUALS_RECORDS(assert_record, r, schema, "compare records");
      freeRecord(assert_record);
    }
  
  TEST_CHECK(closeTable(table));
  TEST_CHECK(deleteTable("test_table_r"));
  TEST_CHECK(shutdownRecordManager());
  free(rids);
  freeRecord(r);
  freeSchema(schema);
  free(table);
  TEST_DONE();
}

void 
testInsertManyRecords(void)
{
  RM_TableData *table = (RM_TableData *) malloc(sizeof(RM_TableData));
  TestRecord inserts[] = { 
    {1, "aaaa", 3}, 
    {2, "bbbb", 2},
    {3, "cccc", 1},
    {4, "dddd", 3},
    {5, "eeee", 5},
    {6, "ffff", 1},
    {7, "gggg", 3},
    {8, "hhhh", 3},
    {9, "iiii", 2},
    {10, "jjjj", 5},
  };
  TestRecord realInserts[10000];
  TestRecord updates[] = {
    {3333, "iiii", 6}
  };
  int numInserts = 10000, i;
  int randomRec = 3333;
  Record *r,*assert_record;
  RID *rids;
  Schema *schema;
  testName = "test creating a new table and inserting 10000 records then updating record from rids[3333]";
  schema = testSchema();
  rids = (RID *) malloc(sizeof(RID) * numInserts);
  
  TEST_CHECK(initRecordManager(NULL));
  TEST_CHECK(createTable("test_table_t",schema));
  TEST_CHECK(openTable(table, "test_table_t"));
  
  // insert rows into table
  for(i = 0; i < numInserts; i++)
    {
      realInserts[i] = inserts[i%10];
      realInserts[i].a = i;
      r = fromTestRecord(schema, realInserts[i]);
      TEST_CHECK(insertRecord(table,r)); 
      rids[i] = r->id;
      freeRecord(r);
    }
  TEST_CHECK(closeTable(table));
  TEST_CHECK(openTable(table, "test_table_t"));

  r = fromTestRecord(schema, realInserts[0]);
  // retrieve records from the table and compare to expected final stage
  for(i = 0; i < numInserts; i++)
    {
      RID rid = rids[i];
      TEST_CHECK(getRecord(table, rid, r));
      assert_record = fromTestRecord(schema, realInserts[i]);      
      ASSERT_EQUALS_RECORDS(assert_record, r, schema, "compare records");
      freeRecord(assert_record);
    }
  freeRecord(r);
 

  r = fromTestRecord(schema, updates[0]);
  r->id = rids[randomRec];
  TEST_CHECK(updateRecord(table,r));
  TEST_CHECK(getRecord(table, rids[randomRec], r)); 
  assert_record = fromTestRecord(schema, updates[0]);
  ASSERT_EQUALS_RECORDS(assert_record, r, schema, "compare records");
   
  TEST_CHECK(closeTable(table));
  TEST_CHECK(deleteTable("test_table_t"));
  TEST_CHECK(shutdownRecordManager());

  free(rids);
  freeRecord(assert_record);
  freeSchema(schema);
  freeRecord(r);
  free(table);
  TEST_DONE();
}

void testScans (void)
{
  RM_TableData *table = (RM_TableData *) malloc(sizeof(RM_TableData));
  TestRecord inserts[] = { 
    {1, "aaaa", 3}, 
    {2, "bbbb", 2},
    {3, "cccc", 1},
    {4, "dddd", 3},
    {5, "eeee", 5},
    {6, "ffff", 1},
    {7, "gggg", 3},
    {8, "hhhh", 3},
    {9, "iiii", 2},
    {10, "jjjj", 5},
  };
  TestRecord scanOneResult[] = { 
    {3, "cccc", 1},
    {6, "ffff", 1},
  };
  bool foundScan[] = {
    FALSE,
    FALSE
  };
  int numInserts = 10, scanSizeOne = 2, i;
  Record *r,*assert_record;
  RID *rids;
  Schema *schema;
  RM_ScanHandle *sc = (RM_ScanHandle *) malloc(sizeof(RM_ScanHandle));
  Expr *sel, *left, *right;
  int rc;

  testName = "test creating a new table and inserting tuples";
  schema = testSchema();
  rids = (RID *) malloc(sizeof(RID) * numInserts);
  
  TEST_CHECK(initRecordManager(NULL));
  TEST_CHECK(createTable("test_table_r",schema));
  TEST_CHECK(openTable(table, "test_table_r"));
  
  // insert rows into table
  for(i = 0; i < numInserts; i++)
  {
      r = fromTestRecord(schema, inserts[i]);
      TEST_CHECK(insertRecord(table,r)); 
      rids[i] = r->id;
      freeRecord(r);
  }

  TEST_CHECK(closeTable(table));
  TEST_CHECK(openTable(table, "test_table_r"));

  // run some scans
  MAKE_CONS(left, stringToValue("i1"));
  MAKE_ATTRREF(right, 2);
  MAKE_BINOP_EXPR(sel, left, right, OP_COMP_EQUAL);

  r = fromTestRecord(schema, inserts[0]);
  TEST_CHECK(startScan(table, sc, sel));
  while((rc = next(sc, r)) == RC_OK)
  {
      for(i = 0; i < scanSizeOne; i++)
      {
          assert_record = fromTestRecord(schema, scanOneResult[i]); 
          if (memcmp(assert_record->data,r->data,getRecordSize(schema)) == 0)
              foundScan[i] = TRUE;
          freeRecord(assert_record);
      }
  }
  freeRecord(r);
  if (rc != RC_RM_NO_MORE_TUPLES)
    TEST_CHECK(rc);
  TEST_CHECK(closeScan(sc));
  for(i = 0; i < scanSizeOne; i++)
    ASSERT_TRUE(foundScan[i], "check for scan result");
  
  // clean up
  TEST_CHECK(closeTable(table));
  TEST_CHECK(deleteTable("test_table_r"));
  TEST_CHECK(shutdownRecordManager());

  free(rids);
  free(table);
  free(sc);
  freeSchema(schema);
  freeExpr(sel);
  TEST_DONE();
}


void testScansTwo (void)
{
  RM_TableData *table = (RM_TableData *) malloc(sizeof(RM_TableData));
  TestRecord inserts[] = { 
    {1, "aaaa", 3}, 
    {2, "bbbb", 2},
    {3, "cccc", 1},
    {4, "dddd", 3},
    {5, "eeee", 5},
    {6, "ffff", 1},
    {7, "gggg", 3},
    {8, "hhhh", 3},
    {9, "iiii", 2},
    {10, "jjjj", 5},
  };
  bool foundScan[] = {
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE
  };
  int numInserts = 10, i;
  Record *r,*assert_record;
  RID *rids;
  Schema *schema;
  RM_ScanHandle *sc = (RM_ScanHandle *) malloc(sizeof(RM_ScanHandle));
  Expr *sel, *left, *right, *first, *se;
  int rc;

  testName = "test creating a new table and inserting tuples";
  schema = testSchema();
  rids = (RID *) malloc(sizeof(RID) * numInserts);
  
  TEST_CHECK(initRecordManager(NULL));
  TEST_CHECK(createTable("test_table_r",schema));
  TEST_CHECK(openTable(table, "test_table_r"));
  
  // insert rows into table
  for(i = 0; i < numInserts; i++)
  {
    r = fromTestRecord(schema, inserts[i]);
    TEST_CHECK(insertRecord(table,r)); 
    rids[i] = r->id;
    freeRecord(r);
  }

  TEST_CHECK(closeTable(table));
  TEST_CHECK(openTable(table, "test_table_r"));

  createRecord(&r, schema);
  // Select 1 record with INT in condition a=2.
  MAKE_CONS(left, stringToValue("i2"));
  MAKE_ATTRREF(right, 0);
  MAKE_BINOP_EXPR(sel, left, right, OP_COMP_EQUAL);
  TEST_CHECK(startScan(table, sc, sel));
  while((rc = next(sc, r)) == RC_OK)
  {
     assert_record = fromTestRecord(schema, inserts[1]);
     ASSERT_EQUALS_RECORDS(assert_record, r, schema, "compare records");
     freeRecord(assert_record);
  }
  if (rc != RC_RM_NO_MORE_TUPLES)
    TEST_CHECK(rc);
  TEST_CHECK(closeScan(sc));
  freeExpr(sel);

  // Select 1 record with STRING in condition b='ffff'.
  MAKE_CONS(left, stringToValue("sffff"));
  MAKE_ATTRREF(right, 1);
  MAKE_BINOP_EXPR(sel, left, right, OP_COMP_EQUAL);
 
  TEST_CHECK(startScan(table, sc, sel));
  while((rc = next(sc, r)) == RC_OK)
  {
     assert_record = fromTestRecord(schema, inserts[5]);
     ASSERT_EQUALS_RECORDS(assert_record, r, schema, "compare records");
     freeRecord(assert_record);
  }
  if (rc != RC_RM_NO_MORE_TUPLES)
    TEST_CHECK(rc);
  TEST_CHECK(closeScan(sc));
  freeExpr(sel);

  // Select all records, with condition being false
  MAKE_CONS(left, stringToValue("i4"));
  MAKE_ATTRREF(right, 2);
  MAKE_BINOP_EXPR(first, right, left, OP_COMP_SMALLER);
  MAKE_UNOP_EXPR(se, first, OP_BOOL_NOT);
  TEST_CHECK(startScan(table, sc, se));
    while((rc = next(sc, r)) == RC_OK)
    {
     for(i = 0; i < numInserts; i++)
     {
      assert_record = fromTestRecord(schema, inserts[i]);
       if (memcmp(assert_record->data,r->data,getRecordSize(schema)) == 0)
	     foundScan[i] = TRUE;
      freeRecord(assert_record);
     }
    }
  if (rc != RC_RM_NO_MORE_TUPLES)
    TEST_CHECK(rc);
  TEST_CHECK(closeScan(sc));
  
  ASSERT_TRUE(!foundScan[0], "not greater than four");
  ASSERT_TRUE(foundScan[4], "greater than four");
  ASSERT_TRUE(foundScan[9], "greater than four");

  // clean up
  TEST_CHECK(closeTable(table));
  TEST_CHECK(deleteTable("test_table_r"));
  TEST_CHECK(shutdownRecordManager());

  freeSchema(schema);
  freeRecord(r);
  free(table);
  free(sc);
  free(rids);
  freeExpr(se);
  TEST_DONE();
}

void 
testUpdateScan (void)
{
  RM_TableData *table = (RM_TableData *) malloc(sizeof(RM_TableData));
  TestRecord inserts[] = { 
    {1, "aaaa", 3}, 
    {2, "bbbb", 2},
    {3, "cccc", 1},
    {4, "dddd", 3},
    {5, "eeee", 5},
    {6, "ffff", 1},
    {7, "gggg", 3},
    {8, "hhhh", 3},
    {9, "iiii", 2},
    {10, "jjjj", 5},
  };
  TestRecord scanOneResult[] = { 
    {3, "cccc", 20},
    {6, "ffff", 20},
  };
  bool foundScan[] = {
    FALSE,
    FALSE
  };
  int numInserts = 10, scanSizeOne = 2, i;
  Record *r,*assert_record;
  RID *rids;
  Schema *schema;
  RM_ScanHandle *sc = (RM_ScanHandle *) malloc(sizeof(RM_ScanHandle));
  Expr *sel, *left, *right;
  int rc;
  testName = "test creating a new table and inserting tuples";
  schema = testSchema();
  rids = (RID *) malloc(sizeof(RID) * numInserts);
  
  TEST_CHECK(initRecordManager(NULL));
  TEST_CHECK(createTable("test_table_r",schema));
  TEST_CHECK(openTable(table, "test_table_r"));
  
  // insert rows into table
  for(i = 0; i < numInserts; i++)
  {
      r = fromTestRecord(schema, inserts[i]);
      TEST_CHECK(insertRecord(table,r)); 
      rids[i] = r->id;
      freeRecord(r);
  }

  TEST_CHECK(closeTable(table));
  createRecord(&r, schema);
  TEST_CHECK(openTable(table, "test_table_r"));

  // run some scans
  MAKE_CONS(left, stringToValue("i1"));
  MAKE_ATTRREF(right, 2);
  MAKE_BINOP_EXPR(sel, left, right, OP_COMP_EQUAL);

  TEST_CHECK(updateScan(table,sel,updateTuple));
  freeExpr(sel);

  MAKE_CONS(left, stringToValue("i20"));
  MAKE_ATTRREF(right, 2);
  MAKE_BINOP_EXPR(sel, left, right, OP_COMP_EQUAL);

  TEST_CHECK(startScan(table, sc, sel));
  while((rc = next(sc, r)) == RC_OK)
  {
      for(i = 0; i < scanSizeOne; i++)
      {
          assert_record = fromTestRecord(schema, scanOneResult[i]);
          if (memcmp(assert_record->data,r->data,getRecordSize(schema)) == 0)
              foundScan[i] = TRUE;
          freeRecord(assert_record);
      }
  }
  if (rc != RC_RM_NO_MORE_TUPLES)
    TEST_CHECK(rc);
  TEST_CHECK(closeScan(sc));

  for(i = 0; i < scanSizeOne; i++)
    ASSERT_TRUE(foundScan[i], "check for update scan result");

  // clean up
  TEST_CHECK(closeTable(table));
  TEST_CHECK(deleteTable("test_table_r"));
  TEST_CHECK(shutdownRecordManager());

  freeRecord(r);
  freeSchema(schema);
  free(table);
  free(sc);
  free(rids);
  freeExpr(sel);
  TEST_DONE();
}

RC
updateTuple(RM_TableData *table, Record *record)
{  
  Value *value = stringToValue("i20");
  setAttr (record, table->schema, 2, value);
  freeVal(value);
  return RC_OK;
}

Schema *
testSchema (void)
{
  Schema *result;
  char *names[] = { "a", "b", "c" };
  DataType dt[] = { DT_INT, DT_STRING, DT_INT };
  int sizes[] = { 0, 4, 0 };
  int keys[] = {0};
  int i;
  char **cpNames = (char **) malloc(sizeof(char*) * 3);
  DataType *cpDt = (DataType *) malloc(sizeof(DataType) * 3);
  int *cpSizes = (int *) malloc(sizeof(int) * 3);
  int *cpKeys = (int *) malloc(sizeof(int));

  for(i = 0; i < 3; i++)
    {
      cpNames[i] = (char *) malloc(2);
      strcpy(cpNames[i], names[i]);
    }
  memcpy(cpDt, dt, sizeof(DataType) * 3);
  memcpy(cpSizes, sizes, sizeof(int) * 3);
  memcpy(cpKeys, keys, sizeof(int));

  result = createSchema(3, cpNames, cpDt, cpSizes, 1, cpKeys);

  return result;
}

void
testUnion (void)
{
  RC rc;
  RM_TableData union_rel;
  RM_TableData *table = (RM_TableData *) malloc(sizeof(RM_TableData));
  TestRecord inserts[] = { 
    {1, "aaaa", 3}, 
    {2, "bbbb", 2},
    {3, "cccc", 1},
    {4, "dddd", 3},
    {5, "eeee", 5},
    {6, "ffff", 1},
    {7, "gggg", 3},
    {8, "hhhh", 3},
    {9, "iiii", 2}
  };
  
  Value *value = NULL;
  int count=0;
  int numInserts = 9, i;
  Record *r;
  RID *rids;
  Schema *schema;
  testName = "test creating new tables and applying union to the tables";
  schema = testSchema();
  rids = (RID *) malloc(sizeof(RID) * numInserts);
  RM_ScanHandle *sc = (RM_ScanHandle *) malloc(sizeof(RM_ScanHandle));
  Expr *sel, *left, *right;

  TEST_CHECK(initRecordManager(NULL));
  TEST_CHECK(createTable("test_table_r",schema));
  TEST_CHECK(openTable(table, "test_table_r"));
  
  // insert rows into table
  for(i = 0; i < numInserts; i++)
    {
      r = fromTestRecord(schema, inserts[i]);
      TEST_CHECK(insertRecord(table,r)); 
      rids[i] = r->id;
      freeRecord(r);
    }

  Union(table,table,0,0);
  TEST_CHECK(openTable(&union_rel, "test_table_r_union_test_table_r"));

  r = fromTestRecord(schema, inserts[0]);
    for(i = 0; i < numInserts; i++)
    {
      count = 0;
      value = malloc(sizeof(Value));
      value->dt = DT_INT;
      value->v.intV = inserts[i].a;
      MAKE_CONS(left, value);
      MAKE_ATTRREF(right, 0);
      MAKE_BINOP_EXPR(sel, left, right, OP_COMP_EQUAL);

      TEST_CHECK(startScan(&union_rel, sc, sel));
      while((rc = next(sc, r)) == RC_OK)
      {
        ++count;
      }  
      if (rc != RC_RM_NO_MORE_TUPLES)
        TEST_CHECK(rc);
      TEST_CHECK(closeScan(sc));
      ASSERT_TRUE(count==2,"Found two records for each one");
      freeExpr(sel);
    }
    
  TEST_CHECK(closeTable(table));
  TEST_CHECK(closeTable(&union_rel));
  TEST_CHECK(deleteTable("test_table_r"));
  TEST_CHECK(deleteTable("test_table_r_union_test_table_r"));
  TEST_CHECK(shutdownRecordManager());

  free(sc);
  freeRecord(r);
  freeSchema(schema);
  free(rids);
  free(table);
  TEST_DONE();
}

void
testNestedLoopJoin (void)
{
  RC rc;
  RM_TableData union_rel;
  RM_TableData *table = (RM_TableData *) malloc(sizeof(RM_TableData));
  TestRecord inserts[] = { 
    {1, "aaaa", 3}, 
    {2, "bbbb", 2},
    {3, "aaaa", 3},
    {4, "aaaa", 3}
  };
  
  Value *value = NULL;
  int count=0;
  int numInserts = 4, i;
  Record *r;
  RID *rids;
  Schema *schema;
  testName = "test creating and table applying join";
  schema = testSchema();
  rids = (RID *) malloc(sizeof(RID) * numInserts);
  RM_ScanHandle *sc = (RM_ScanHandle *) malloc(sizeof(RM_ScanHandle));
  Expr *sel, *left, *right;

  TEST_CHECK(initRecordManager(NULL));
  TEST_CHECK(createTable("test_table_r",schema));
  TEST_CHECK(openTable(table, "test_table_r"));
  
  // insert rows into table
  for(i = 0; i < numInserts; i++)
    {
      r = fromTestRecord(schema, inserts[i]);
      TEST_CHECK(insertRecord(table,r)); 
      rids[i] = r->id;
      freeRecord(r);
    }

    Nested_Loop_Join(table,table);
    TEST_CHECK(openTable(&union_rel, "test_table_r_join_test_table_r"));
    r = fromTestRecord(union_rel.schema, inserts[0]);
    for(i = 0; i < numInserts; i++)
    {
      count = 0;
      value = malloc(sizeof(Value));
      value->dt = DT_INT;
      value->v.intV = inserts[i].a;
      MAKE_CONS(left, value);
      MAKE_ATTRREF(right, 0);
      MAKE_BINOP_EXPR(sel, left, right, OP_COMP_EQUAL);

      TEST_CHECK(startScan(&union_rel, sc, sel));
      while((rc = next(sc, r)) == RC_OK)
      {
        ++count;
      }  
      if (rc != RC_RM_NO_MORE_TUPLES)
        TEST_CHECK(rc);
      TEST_CHECK(closeScan(sc));
      ASSERT_TRUE(count==4,"Found four records for each one");
      freeExpr(sel);
    }
  
  TEST_CHECK(closeTable(table));
  TEST_CHECK(closeTable(&union_rel));
  TEST_CHECK(deleteTable("test_table_r"));
  TEST_CHECK(deleteTable("test_table_r_join_test_table_r"));
  TEST_CHECK(shutdownRecordManager());

  free(sc);
  freeRecord(r);
  freeSchema(schema);
  free(rids);
  free(table);
  TEST_DONE();
}


Record *
fromTestRecord (Schema *schema, TestRecord in)
{
  return testRecord(schema, in.a, in.b, in.c);
}

Record *
testRecord(Schema *schema, int a, char *b, int c)
{
  Record *result;
  Value *value;

  TEST_CHECK(createRecord(&result, schema));

  MAKE_VALUE(value, DT_INT, a);
  TEST_CHECK(setAttr(result, schema, 0, value));
  freeVal(value);

  MAKE_STRING_VALUE(value, b);
  TEST_CHECK(setAttr(result, schema, 1, value));
  freeVal(value);

  MAKE_VALUE(value, DT_INT, c);
  TEST_CHECK(setAttr(result, schema, 2, value));
  freeVal(value);

  return result;
}
