#ifndef OPTIONALS_H
#define OPTIONALS_H

#include "record_mgr.h"

RC Union(RM_TableData *rel1, RM_TableData *rel2, int attr1, int attr2);
RC Nested_Loop_Join(RM_TableData *rel1, RM_TableData *rel2);
#endif