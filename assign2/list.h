#include "dt.h"
#ifndef LIST_H
#define LIST_H

/************************************************************
 *                         Macros                           *
 ************************************************************/
#define OffsetOf(type, memberName) \
    ((char *) &(((type *)(0))->memberName))

#define GetEnclosingTypePtr(type, memberName, nodePtr) \
    ((type *) ((char *)nodePtr - OffsetOf(type, memberName)))

#define FOR_EACH(type, memberName, headPtr, typePtr) \
    ListNode * node;\
    for (node = headPtr->next; node != headPtr; node = node->next){ \
         typePtr = GetEnclosingTypePtr(type, memberName, node);\

/************************************************************
 *                    Data structures                       *
 ************************************************************/


typedef struct ListNode {
    struct ListNode * prev;
    struct ListNode * next;
}ListNode;


/************************************************************
 *                    Interface                             *
 ************************************************************/
void InitListNode(ListNode * node);
void AddToFront(ListNode *node1, ListNode *node2);
void AddToBack(ListNode *node1, ListNode *node2);
void RemoveFromList(ListNode *node);
bool IsEmpty(ListNode* head);

#endif
