﻿========================================================================= Buffer Manager
Written by Abrar Shivani (ashivani [at] hawk.iit.edu).
=========================================================================


=========================================================================
Build and Run
=========================================================================
To compile the code, run the following command:


> make


Next, run the following command


> ./test_assign2


This will run test code for buffer manager.


To revert the compilation, run following command:
> make clean




=========================================================================
Valgrind Results
=========================================================================


HEAP SUMMARY:
in use at exit: 0 bytes in 0 blocks
total heap usage: 50,983 allocs, 50,983 frees, 208,713,354 bytes allocated
All heap blocks were freed -- no leaks are possible


ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)


=========================================================================
Design
=========================================================================


Data Structures:


1. Link List
The design of our linked list is based on linked lists in the linux kernel. The basic idea is to have a doubly circular linked list with a node structure that contains only the next and previous pointers. This node structure can then be used as the list head as well as embedded into other structures that need to be part of a list. All list operations work on this node structure consisting of the two pointers and the pointers point to these node structures itself. The pointer to the structures that embed these node structures can be obtained easily by computing the offset of the member identifying the node structure within the encapsulating structure.
This design allows us to keep the linked list generic so that it can be used for any type of structure and also allows us to keep a particular structure in multiple lists at the same time. This design also makes it possible to to add and remove structures from the list without allocating memory for the node structures separately since they get allocated along with the encapsulating structure. The linked list module also provides a convenient FOR_EACH() macro that makes it really easy to traverse a list. 


1. Hash table
The hash table is built using the above mentioned linked list module. It consists of an array of list heads representing hash buckets. Each list head in the array represents a hash bucket that can contain multiple items. Items are added to a hash bucket by adding the item to the appropriate list head. We use a simple modulo based hash function to distribute the items to different hash buckets.
As the hash table is built using a generic linked list, the hash table is also generic and allows us to create hash tables for any kind of structure. And we also get the benefit of not having to allocate a hash node every time we insert or remove an item into the hash table.


1. PageHead
There is a PageHead for every frame in the buffer pool. This PageHead stores metadata for every page frame in the pool. It also has a pointer to the appropriate page frame. All buffer pool operations use the PageHead and only access the appropriate page frame when needed for example for reading a page, for writing a page, etc. This makes is really easy to track metadata about each page frame in the buffer pool.


1. MgmtData
Each buffer pool maintains some additional information that is stored within this mgmt data structure with the buffer pool structure. This mgmt data structure stores pointers to PageFrames and PageHeads within the buffer pool that are allocated during the buffer pool startup and are freed during the buffer pool shutdown. This helps us avoid allocations and deallocations during normal operations of the buffer pool. This mgmt data structure also contains other information that is required for allocation, eviction, eviction algorithms and for maintaining some buffer pool statistics.
        


Loading a new page into buffer pool:
* Two functions are used here: allocate() and evict()
* allocate() is called first. If there are empty page frames in the buffer pool, allocate() will allocate one of those so that we can load the page into it. If buffer pool is full, allocate() will return error and we will have to evict(). Note that, once buffer pool is full, it will always stay full.
* If allocate() returns error, evict() is called to find a candidate page frame to evict so that we can load a new page into this page frame.




Eviction algorithm abstraction:
All eviction algorithms are abstracted out into 3 functions:
1. evictAlgoPostPin()
This is called to process any eviction algorithm specific code after we pin a page into a page frame.
1. evictAlgoPostUnpin()
This is called to process any eviction algorithm code after we unpin a page.
1. evict()
        This is called to find an eviction candidate from all the page frames in the buffer pool.


Eviction Algorithms:
1. LFU
For LFU, we maintain an access count in PageHead which is incremented on every pin request. Then in evict(), we search for a page frame that has the least access count.
1. FIFO
For FIFO, we maintain a list of PageHeads. PageHeads are added to the list on load and removed during eviction. The first PageHead in the list is the first one that was loaded so is the first candidate to be considered for eviction.
1. LRU
We maintain a list similar to FIFO but it tracks the PageHeads in the least recently used order. The first element in the list is the least recently used PageHead. As an optimization we add to the list on unpin of a page if the fixcount drops to zero. And we remove the PageHead when we pin a page in the buffer pool. This allows us to evict from the buffer pool without scanning the entire LRU list to find PageHeads with fixedcount of zero.
1. CLOCK
We keep a clock bit flag for the this algorithm in every PageHead. Whenever a page is unpinned we set the flag. We do it in unpin for the same reason as described in LRU. We also have a clock pointer in the buffer pool mgmt data structure. On evict(), we move the clock pointer forward. If we find a PageHead with the flag set we unset it and move forward. If we find a PageHead with the flag unset, we pick that page frame for eviction.








Statistics:
We maintain some variables and arrays in the buffer pool mgmt data structure that allow us to return these statistics without having to scan all the PageHeads. For example, we maintain an array of the dirty bits of all PageHeads in the buffer pool.






Thread Safety:
The entire buffer pool is thread safe. We maintain a mutex in the buffer pool mgmt data structure and acquire the lock for all buffer pool operations and release the lock on completion of the operation.










=========================================================================
Implementation
=========================================================================




Functions:


pinPage 
It pins the page with page number pageNum. The buffer manager is responsible to set the pageNum field of the page handle passed to the method. Similarly, the data field should point to the page frame the page is stored in (the area in memory storing the content of the page).


unpinPage 
unpins the page page. The pageNum field of page is used to figure out which page to unpin.


markDirty 
marks a page as dirty.


forcePage 
writes the current content of the page back to the page file on disk.


initBufferPool
 Creates a new buffer pool with numPages page frames 
 using the page replacement strategy strategy. 
 The pool is used to cache pages from the page file with name pageFileName.