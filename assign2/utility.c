#include "utility.h"
#include <strings.h>
#include <stdlib.h>

/* Helper functions */
SM_PageHandle createPage(){
	SM_PageHandle page;
	page = malloc(sizeof(char)*PAGE_SIZE);
	bzero(page,PAGE_SIZE);
	return page;
}

void deletePage(SM_PageHandle page){	
	free(page);
}
